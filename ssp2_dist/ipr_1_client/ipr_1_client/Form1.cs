﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Sockets;


namespace ipr_1_client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            TcpClient client = new TcpClient("localhost", 50004);
            try
            {
                using (var stream = client.GetStream())
                {
                    int fNameLen = stream.ReadByte();
                    byte[] fNameBytes = new byte[fNameLen];

                    stream.Read(fNameBytes, 0, fNameLen);
                    string fName = Encoding.Unicode.GetString(fNameBytes);

                    using (var fs = File.OpenWrite(fName))
                    {
                        byte[] buffer = new byte[1024];
                        while (true)
                        {
                            int r = stream.Read(buffer, 0, 1024);
                            if (r == 0)
                                break;

                            fs.Write(buffer, 0, r);
                        }
                        fs.Flush();

                        pBox.ImageLocation = fName;
                    }
                }
            }

            finally
            {
                client.Close();
            }

        }
       
    }
}
