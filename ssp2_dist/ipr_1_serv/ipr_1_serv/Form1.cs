﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace ipr_1_serv
{

    public partial class Form1 : Form
    {
        NetworkStream ns;
        String fileName;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse image",
                CheckFileExists = true,
                CheckPathExists = true,
                DefaultExt = "jpg",
                RestoreDirectory = true,
                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pBox.ImageLocation = openFileDialog.FileName;
                fileName = openFileDialog.FileName;
            }
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            var listener = new TcpListener(50004);
            listener.Start();

            while (true)
            {
                var client = listener.AcceptTcpClient();
                ThreadPool.QueueUserWorkItem(cb => ClientThread(client));
            }
        }

        void ClientThread(TcpClient client)
        {
            using (var cs = client.GetStream())
            {
                byte[] fNameBytes = Encoding.Unicode.GetBytes(Path.GetFileName(fileName));
                cs.WriteByte((byte)fNameBytes.Length);
                cs.Write(fNameBytes, 0, fNameBytes.Length);

                using (var fs = File.OpenRead(fileName))
                {
                    byte[] buffer = new byte[1024];
                    while (true)
                    {
                        int r = fs.Read(buffer, 0, 1024);
                        if (r == 0)
                            break;

                        cs.Write(buffer, 0, r);
                    }
                }
            }

            client.Close();
        }
    }
}