﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ipr_2_calc
{
    public partial class UserControl1: UserControl
    {
        double value = 0;

        public UserControl1()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            setValue(0);
        }

        public void setValue(double val)
        {
            value = val;
            tvResult.Text = "= " + value;
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            setValue(Double.Parse(etNewVal.Text) + value); 
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            setValue(value - Double.Parse(etNewVal.Text));
        }

        private void btnMult_Click(object sender, EventArgs e)
        {
            setValue(value * Double.Parse(etNewVal.Text));
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            setValue(value / Double.Parse(etNewVal.Text));
        }
    }
}
