﻿namespace SolarSystemVasKop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPlay = new System.Windows.Forms.Button();
            this.imgMoon = new System.Windows.Forms.PictureBox();
            this.imgEarth = new System.Windows.Forms.PictureBox();
            this.imgSun = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgMoon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEarth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSun)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(23, 25);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(75, 23);
            this.btnPlay.TabIndex = 3;
            this.btnPlay.Text = "|>";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // imgMoon
            // 
            this.imgMoon.Image = global::SolarSystemVasKop.Properties.Resources.moon;
            this.imgMoon.Location = new System.Drawing.Point(907, 141);
            this.imgMoon.Name = "imgMoon";
            this.imgMoon.Size = new System.Drawing.Size(50, 50);
            this.imgMoon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMoon.TabIndex = 2;
            this.imgMoon.TabStop = false;
            // 
            // imgEarth
            // 
            this.imgEarth.Image = global::SolarSystemVasKop.Properties.Resources.flatearth;
            this.imgEarth.Location = new System.Drawing.Point(829, 201);
            this.imgEarth.Name = "imgEarth";
            this.imgEarth.Size = new System.Drawing.Size(70, 70);
            this.imgEarth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgEarth.TabIndex = 1;
            this.imgEarth.TabStop = false;
            // 
            // imgSun
            // 
            this.imgSun.Image = global::SolarSystemVasKop.Properties.Resources.solnyshko;
            this.imgSun.Location = new System.Drawing.Point(472, 228);
            this.imgSun.Name = "imgSun";
            this.imgSun.Size = new System.Drawing.Size(200, 200);
            this.imgSun.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgSun.TabIndex = 0;
            this.imgSun.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1157, 625);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.imgMoon);
            this.Controls.Add(this.imgEarth);
            this.Controls.Add(this.imgSun);
            this.Name = "Form1";
            this.Text = "Solar system model";
            ((System.ComponentModel.ISupportInitialize)(this.imgMoon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEarth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSun)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgSun;
        private System.Windows.Forms.PictureBox imgEarth;
        private System.Windows.Forms.PictureBox imgMoon;
        private System.Windows.Forms.Button btnPlay;
    }
}

