﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SolarSystemVasKop
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private bool isAnimated = false;
        private Thread earthThread;
        private Thread moonThread;

        private void btnPlay_Click(object sender, EventArgs e)
        {
            isAnimated = !isAnimated;
            if (isAnimated)
            {
                btnPlay.Text = "| |";
                moonThread = new Thread(() => circleLoop(imgMoon, imgEarth, 0.1, 150.0));
                earthThread = new Thread(() => circleLoop(imgEarth, imgSun, 0.05, 300.0));
                earthThread.Start();
                moonThread.Start();
            } else
            {
                btnPlay.Text = "|>";
                moonThread.Abort();
                earthThread.Abort();
                moonThread = null;
                earthThread = null;
            }
        }

        private void circleLoop(PictureBox target, PictureBox anchor, double speed, double radius)
        {
            int i = 0;
            while (i < 50000)
            {
                this.Invalidate();
                Thread.Sleep(50);
                i++;
                this.Invoke((MethodInvoker)(() =>
                {
                    KeyValuePair<int, int> coords = getPathCoords(speed, radius, i);
                    KeyValuePair<int, int> anchorPos = getCenter(anchor);
                    target.Location = new Point(anchorPos.Key + coords.Key - target.Height/2, anchorPos.Value + coords.Value - target.Width/2);
                }));

                continue;
            }
        }

        private KeyValuePair<int, int> getCenter(PictureBox o)
        {
            return new KeyValuePair<int, int>(o.Location.X + o.Width/2, o.Location.Y + o.Height/2);
        }

        private KeyValuePair<int, int> getPathCoords(double speed, double radius, int x)
        {
            return new KeyValuePair<int, int>(Convert.ToInt32(radius * Math.Sin(x * speed)), Convert.ToInt32(radius * Math.Cos(x * speed)));
        }
    }
}
