﻿namespace SqlServVasKop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbServer = new System.Windows.Forms.TextBox();
            this.tbBase = new System.Windows.Forms.TextBox();
            this.tbUser = new System.Windows.Forms.TextBox();
            this.tbPass = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.tbAllSongs = new System.Windows.Forms.Button();
            this.btnViewSongsLarger = new System.Windows.Forms.Button();
            this.tbLargerThan = new System.Windows.Forms.TextBox();
            this.tbSongName = new System.Windows.Forms.TextBox();
            this.tbSongDuration = new System.Windows.Forms.TextBox();
            this.tbSongArtist = new System.Windows.Forms.TextBox();
            this.btnAddSong = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbServer
            // 
            this.tbServer.Location = new System.Drawing.Point(606, 337);
            this.tbServer.Name = "tbServer";
            this.tbServer.Size = new System.Drawing.Size(195, 20);
            this.tbServer.TabIndex = 0;
            this.tbServer.Text = "MGKIRIL-PC\\SQLEXPRESS";
            // 
            // tbBase
            // 
            this.tbBase.Location = new System.Drawing.Point(606, 364);
            this.tbBase.Name = "tbBase";
            this.tbBase.Size = new System.Drawing.Size(195, 20);
            this.tbBase.TabIndex = 1;
            this.tbBase.Text = "baseone";
            // 
            // tbUser
            // 
            this.tbUser.Location = new System.Drawing.Point(606, 391);
            this.tbUser.Name = "tbUser";
            this.tbUser.Size = new System.Drawing.Size(195, 20);
            this.tbUser.TabIndex = 2;
            this.tbUser.Text = "nuer";
            // 
            // tbPass
            // 
            this.tbPass.Location = new System.Drawing.Point(606, 418);
            this.tbPass.Name = "tbPass";
            this.tbPass.Size = new System.Drawing.Size(195, 20);
            this.tbPass.TabIndex = 3;
            this.tbPass.Text = "zzzzzzzz";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(496, 364);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 4;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(493, 398);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(55, 13);
            this.labelStatus.TabIndex = 5;
            this.labelStatus.Text = "Status: no";
            // 
            // tbAllSongs
            // 
            this.tbAllSongs.Location = new System.Drawing.Point(42, 41);
            this.tbAllSongs.Name = "tbAllSongs";
            this.tbAllSongs.Size = new System.Drawing.Size(210, 30);
            this.tbAllSongs.TabIndex = 6;
            this.tbAllSongs.Text = "View all songs";
            this.tbAllSongs.UseVisualStyleBackColor = true;
            this.tbAllSongs.Click += new System.EventHandler(this.tbAllSongs_Click);
            // 
            // btnViewSongsLarger
            // 
            this.btnViewSongsLarger.Location = new System.Drawing.Point(42, 98);
            this.btnViewSongsLarger.Name = "btnViewSongsLarger";
            this.btnViewSongsLarger.Size = new System.Drawing.Size(210, 27);
            this.btnViewSongsLarger.TabIndex = 7;
            this.btnViewSongsLarger.Text = "View songs larger than...";
            this.btnViewSongsLarger.UseVisualStyleBackColor = true;
            this.btnViewSongsLarger.Click += new System.EventHandler(this.btnViewSongsLarger_Click);
            // 
            // tbLargerThan
            // 
            this.tbLargerThan.Location = new System.Drawing.Point(306, 104);
            this.tbLargerThan.Name = "tbLargerThan";
            this.tbLargerThan.Size = new System.Drawing.Size(100, 20);
            this.tbLargerThan.TabIndex = 8;
            this.tbLargerThan.Text = "160";
            // 
            // tbSongName
            // 
            this.tbSongName.Location = new System.Drawing.Point(53, 207);
            this.tbSongName.Name = "tbSongName";
            this.tbSongName.Size = new System.Drawing.Size(100, 20);
            this.tbSongName.TabIndex = 9;
            // 
            // tbSongDuration
            // 
            this.tbSongDuration.Location = new System.Drawing.Point(306, 207);
            this.tbSongDuration.Name = "tbSongDuration";
            this.tbSongDuration.Size = new System.Drawing.Size(100, 20);
            this.tbSongDuration.TabIndex = 10;
            // 
            // tbSongArtist
            // 
            this.tbSongArtist.Location = new System.Drawing.Point(177, 207);
            this.tbSongArtist.Name = "tbSongArtist";
            this.tbSongArtist.Size = new System.Drawing.Size(100, 20);
            this.tbSongArtist.TabIndex = 11;
            // 
            // btnAddSong
            // 
            this.btnAddSong.Location = new System.Drawing.Point(42, 160);
            this.btnAddSong.Name = "btnAddSong";
            this.btnAddSong.Size = new System.Drawing.Size(210, 30);
            this.btnAddSong.TabIndex = 12;
            this.btnAddSong.Text = "Add song";
            this.btnAddSong.UseVisualStyleBackColor = true;
            this.btnAddSong.Click += new System.EventHandler(this.btnAddSong_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(220, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "artist";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(347, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "duration";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddSong);
            this.Controls.Add(this.tbSongArtist);
            this.Controls.Add(this.tbSongDuration);
            this.Controls.Add(this.tbSongName);
            this.Controls.Add(this.tbLargerThan);
            this.Controls.Add(this.btnViewSongsLarger);
            this.Controls.Add(this.tbAllSongs);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.tbPass);
            this.Controls.Add(this.tbUser);
            this.Controls.Add(this.tbBase);
            this.Controls.Add(this.tbServer);
            this.Name = "Form1";
            this.Text = "Songs browser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.TextBox tbBase;
        private System.Windows.Forms.TextBox tbUser;
        private System.Windows.Forms.TextBox tbPass;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button tbAllSongs;
        private System.Windows.Forms.Button btnViewSongsLarger;
        private System.Windows.Forms.TextBox tbLargerThan;
        private System.Windows.Forms.TextBox tbSongName;
        private System.Windows.Forms.TextBox tbSongDuration;
        private System.Windows.Forms.TextBox tbSongArtist;
        private System.Windows.Forms.Button btnAddSong;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

