﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SqlServVasKop
{
    public partial class Form1 : Form
    {
        SqlConnection connection;

        public Form1()
        {
            InitializeComponent();
        }

        ~Form1() {
            if (connection != null)
            {
                connection.Close();
            }
           
        }

        private void btnAddSong_Click(object sender, EventArgs e)
        {
            if (connection == null)
            {
                MessageBox.Show("No connection");
                return;
            }
            String sqlExpression = "AddSong";
            try
            {
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter nameParam = new SqlParameter
                {
                    ParameterName = "@name",
                    Value = tbSongName.Text
                };
                command.Parameters.Add(nameParam);
                SqlParameter artistParam = new SqlParameter
                {
                    ParameterName = "@artist",
                    Value = tbSongArtist.Text
                };
                command.Parameters.Add(artistParam);
                SqlParameter durationParam = new SqlParameter
                {
                    ParameterName = "@duration",
                    Value = tbSongDuration.Text
                };
                command.Parameters.Add(durationParam);
                var result = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured");
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            String connetionString = "Server=" + tbServer.Text + ";Database=" + tbBase.Text + "; User Id = " + tbUser.Text + "; Password = " + tbPass.Text + ";";
            connection = new SqlConnection(connetionString);
            if (connection != null)
            {
                try
                {
                    connection.Open();
                    labelStatus.Text = "Connected";
                } catch (Exception ex)
                {
                    labelStatus.Text = "Connection failed";
                    connection = null;
                }
                
            }
            else
            {
                labelStatus.Text = "Connection failed";
                return;
            }
            
        }

        private void tbAllSongs_Click(object sender, EventArgs e)
        {
            if (connection == null)
            {
                MessageBox.Show("No connection");
                return;
            }

            string sqlExpression = "AllSongs";
            try
            {
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                var reader = command.ExecuteReader();
                string resultString = "";
                if (reader.HasRows)
                {
                    resultString += reader.GetName(0) + " | " + reader.GetName(1) + " | " + reader.GetName(2) + Environment.NewLine;

                    while (reader.Read())
                    {
                        resultString += reader.GetString(0) + " | " + reader.GetString(1) + " | " + reader.GetInt32(2) + Environment.NewLine;
                    }
                }
                reader.Close();
                MessageBox.Show(resultString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured");
            }
        }

        private void btnViewSongsLarger_Click(object sender, EventArgs e)
        {
            if (connection == null)
            {
                MessageBox.Show("No connection");
                return;
            }

            string sqlExpression = "LargeSongs";
            try
            {
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter durationParam = new SqlParameter
                {
                    ParameterName = "@duration",
                    Value = tbLargerThan.Text
                };
                command.Parameters.Add(durationParam);
                var reader = command.ExecuteReader();
                string resultString = "";
                if (reader.HasRows)
                {
                    resultString += reader.GetName(0) + " | " + reader.GetName(1) + " | " + reader.GetName(2) + Environment.NewLine;

                    while (reader.Read())
                    {
                        resultString += reader.GetString(0) + " | " + reader.GetString(1) + " | " + reader.GetInt32(2) + Environment.NewLine;
                    }
                }
                reader.Close();
                MessageBox.Show(resultString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured");
            }
        }
    }
}
