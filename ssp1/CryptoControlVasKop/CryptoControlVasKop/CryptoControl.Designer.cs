﻿
namespace CryptoControlVasKop
{
    partial class CryptoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.etCryptoBox = new System.Windows.Forms.TextBox();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // etCryptoBox
            // 
            this.etCryptoBox.Location = new System.Drawing.Point(3, 0);
            this.etCryptoBox.Multiline = true;
            this.etCryptoBox.Name = "etCryptoBox";
            this.etCryptoBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.etCryptoBox.Size = new System.Drawing.Size(113, 56);
            this.etCryptoBox.TabIndex = 0;
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(122, 3);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(55, 54);
            this.btnEncrypt.TabIndex = 1;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // CryptoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnEncrypt);
            this.Controls.Add(this.etCryptoBox);
            this.Name = "CryptoControl";
            this.Size = new System.Drawing.Size(178, 57);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox etCryptoBox;
        private System.Windows.Forms.Button btnEncrypt;
    }
}
