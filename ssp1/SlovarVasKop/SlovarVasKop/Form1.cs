﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace SlovarVasKop
{
    public partial class Form1 : Form
    {

        class Pair<A, B> {
            A component1;
            B component2;

            public Pair(A a, B b){
                component1 = a;
                component2 = b;
            }

            public A getFirst()
            {
                return component1;
            }

            public B getSecond()
            {
                return component2;
            }
        }

        private List<Pair<String, String>> vocabulary;
        private Pair<String, String> vocabularyName;
        private bool isReversed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSwap_Click(object sender, EventArgs e)
        {
            if (isVocabularyUnavailable()) return;

            isReversed = !isReversed;
            if (isReversed)
            {
                label2.Text = vocabularyName.getFirst();
                label1.Text = vocabularyName.getSecond();
            } else
            {
                label1.Text = vocabularyName.getFirst();
                label2.Text = vocabularyName.getSecond();
            }

            tbFirst.Text = "";
            tbSecond.Text = "";
        }

        private List<Pair<String, String>> loadVocabulary(String path)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    String currLine;
                    String[] currWords;
                    List<Pair<String, String>> vocabulary = new List<Pair<String, String>>();
                    if (!sr.EndOfStream)
                    {
                        currLine = sr.ReadLine();
                        currWords = currLine.Split(':');
                        vocabularyName = new Pair<String, String>(currWords[0], currWords[1]);
                        label1.Text = vocabularyName.getFirst();
                        label2.Text = vocabularyName.getSecond();
                    }
                    while (!sr.EndOfStream)
                    {
                        currLine = sr.ReadLine();
                        currWords = currLine.Split(':');
                        vocabulary.Add(new Pair<String, String>(currWords[0], currWords[1]));
                    }
                    isReversed = false;
                    return vocabulary;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private int Levenstein(string first, string second)
        {
            if (first.Length == 0) return second.Length;
            if (second.Length == 0) return first.Length;
           

            var d = new int[first.Length + 1, second.Length + 1];
            for (var i = 0; i <= first.Length; i++)
            {
                d[i, 0] = i;
            }

            for (var j = 0; j <= second.Length; j++)
            {
                d[0, j] = j;
            }

            for (var i = 1; i <= first.Length; i++)
            {
                for (var j = 1; j <= second.Length; j++)
                {
                    var cost = (second[j - 1] == first[i - 1]) ? 0 : 1;
                    d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + cost);
                }
            }
            return d[first.Length, second.Length];
        }

        private void btnTranslate_Click(object sender, EventArgs e)
        {
            if (isVocabularyUnavailable()) return;

            if (!isReversed)
            {
                int bestDist = Levenstein(vocabulary[0].getFirst(), tbFirst.Text);
                List<Pair<String, String>> bestWords = new List<Pair<string, string>>();
                bestWords.Add(vocabulary[0]);
                int currDist;
                foreach (Pair<String, String> pair in vocabulary)
                {
                    currDist = Levenstein(pair.getFirst(), tbFirst.Text);
                    if (currDist < bestDist)
                    {
                        bestDist = currDist;
                        bestWords.Clear();
                        bestWords.Add(pair);
                    }
                    else if (currDist == bestDist)
                    {
                        bestWords.Add(pair);
                    }
                }
                if (bestDist == 1)
                {
                    String output = "";
                    foreach (Pair<string, string> word in bestWords)
                    {
                        output += word.getFirst() + " - " + word.getSecond() + Environment.NewLine;
                    }
                    tbSecond.Text = output;
                }
                else if (bestDist == 0)
                {
                    tbFirst.Text = bestWords[0].getFirst();
                    String output = "";
                    foreach (Pair<string, string> word in bestWords)
                    {
                        output += word.getSecond() + Environment.NewLine;
                    }
                    tbSecond.Text = output;
                }
                else
                {
                    tbSecond.Text = "404";
                }
            } else
            {
                int bestDist = Levenstein(vocabulary[0].getSecond(), tbFirst.Text);
                List<Pair<String, String>> bestWords = new List<Pair<string, string>>();
                bestWords.Add(vocabulary[0]);
                int currDist;
                foreach (Pair<String, String> pair in vocabulary)
                {
                    currDist = Levenstein(pair.getSecond(), tbFirst.Text);
                    if (currDist < bestDist)
                    {
                        bestDist = currDist;
                        bestWords.Clear();
                        bestWords.Add(pair);
                    } else if (currDist == bestDist)
                    {
                        bestWords.Add(pair);
                    }
                }
                if (bestDist == 1)
                {
                    String output = "";
                    foreach (Pair<string, string> word in bestWords)
                    {
                        output += word.getSecond() + " - " + word.getFirst() + Environment.NewLine;
                    }
                    tbSecond.Text = output;
                }
                else if (bestDist == 0)
                {
                    tbFirst.Text = bestWords[0].getSecond();
                    String output = "";
                    foreach (Pair<string, string> word in bestWords)
                    {
                        output += word.getFirst() + Environment.NewLine;
                    }
                    tbSecond.Text = output;
                }
                else
                {
                    tbSecond.Text = "404";
                }
            }
            
        }

        private void btnVocabulary_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse vocabulary",
                CheckFileExists = true,
                CheckPathExists = true,
                DefaultExt = "txt",
                Filter = "txt files (*.txt)|*.txt",
                RestoreDirectory = true,
                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                vocabulary = loadVocabulary(openFileDialog.FileName);
                isVocabularyUnavailable();
            }

        }

        private bool isVocabularyUnavailable()
        {
            if (vocabulary == null || vocabulary.Count == 0)
            {
                MessageBox.Show("Error while loading vocabulary");
                return true;
            }
            else return false;
        }
        
    }
}
