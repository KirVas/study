﻿namespace SlovarVasKop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSwap = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbFirst = new System.Windows.Forms.TextBox();
            this.tbSecond = new System.Windows.Forms.TextBox();
            this.btnTranslate = new System.Windows.Forms.Button();
            this.btnVocabulary = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSwap
            // 
            this.btnSwap.Location = new System.Drawing.Point(215, 44);
            this.btnSwap.Name = "btnSwap";
            this.btnSwap.Size = new System.Drawing.Size(75, 23);
            this.btnSwap.TabIndex = 0;
            this.btnSwap.Text = "<-- | -->";
            this.btnSwap.UseVisualStyleBackColor = true;
            this.btnSwap.Click += new System.EventHandler(this.btnSwap_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(371, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 2;
            // 
            // tbFirst
            // 
            this.tbFirst.Location = new System.Drawing.Point(81, 107);
            this.tbFirst.Margin = new System.Windows.Forms.Padding(2);
            this.tbFirst.Name = "tbFirst";
            this.tbFirst.Size = new System.Drawing.Size(124, 20);
            this.tbFirst.TabIndex = 3;
            // 
            // tbSecond
            // 
            this.tbSecond.Location = new System.Drawing.Point(328, 87);
            this.tbSecond.Margin = new System.Windows.Forms.Padding(2);
            this.tbSecond.Multiline = true;
            this.tbSecond.Name = "tbSecond";
            this.tbSecond.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbSecond.Size = new System.Drawing.Size(146, 74);
            this.tbSecond.TabIndex = 4;
            // 
            // btnTranslate
            // 
            this.btnTranslate.Location = new System.Drawing.Point(215, 149);
            this.btnTranslate.Margin = new System.Windows.Forms.Padding(2);
            this.btnTranslate.Name = "btnTranslate";
            this.btnTranslate.Size = new System.Drawing.Size(82, 32);
            this.btnTranslate.TabIndex = 5;
            this.btnTranslate.Text = "Translate";
            this.btnTranslate.UseVisualStyleBackColor = true;
            this.btnTranslate.Click += new System.EventHandler(this.btnTranslate_Click);
            // 
            // btnVocabulary
            // 
            this.btnVocabulary.Location = new System.Drawing.Point(408, 199);
            this.btnVocabulary.Name = "btnVocabulary";
            this.btnVocabulary.Size = new System.Drawing.Size(75, 23);
            this.btnVocabulary.TabIndex = 6;
            this.btnVocabulary.Text = "vocabulary";
            this.btnVocabulary.UseVisualStyleBackColor = true;
            this.btnVocabulary.Click += new System.EventHandler(this.btnVocabulary_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 253);
            this.Controls.Add(this.btnVocabulary);
            this.Controls.Add(this.btnTranslate);
            this.Controls.Add(this.tbSecond);
            this.Controls.Add(this.tbFirst);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSwap);
            this.Name = "Form1";
            this.Text = "Vocabulary";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSwap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSecond;
        private System.Windows.Forms.TextBox tbFirst;
        private System.Windows.Forms.Button btnTranslate;
        private System.Windows.Forms.Button btnVocabulary;
    }
}

