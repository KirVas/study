/*****************************************************************************

		Copyright (c) My Company

 Project:  VAASL1
 FileName: VAASL1.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "vaasl1.inc"

predicates
nondeterm obrab
nondeterm raspol(string,string)
nondeterm postav(string,string,string,integer)
nondeterm largeExport(string,integer)
nondeterm repeat
clauses
obrab:- repeat, 
            write("Tovar: "), readln(Item),
            write("Sroimost: "), readint(Value),
            write("Krupnie exporteri: "), nl, largeExport(Item,Value), nl,
            write("Escho? "), readchar(Again),Again='n'.

largeExport(Item,TargetValue):-postav(From,To,Item,Value),raspol(From,FromCountry),raspol(To,ToCountry),
				FromCountry<>ToCountry,Value>TargetValue,
				nl, write(From, ", ", FromCountry, " - ", To, ", ", ToCountry), fail.
largeExport(_,_).
raspol("Sch","RU"). 
raspol("EL","PL"). 
raspol("Frap", "PL").
raspol("Nikel","RU").
raspol("Opt","RU").  
raspol("Ggng","GE").
postav("EL", "Frap", "transform",80).
postav("EL","Nikel", "transform", 60).     
postav("EL","Sch","transform",200).
postav("Frap","Nikel","transform",500).
postav("Grng","Sch","transform",400).
postav("Sch","Opt","oscil",500).
postav("Sch","Opt","transform",300).
postav("Sch","Frap","tranform",500).
repeat.
repeat:-repeat.
goal
obrab.
