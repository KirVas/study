﻿using System;
using System.Data.SqlClient;

namespace IPR_2_BIBD
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter MS SQL Server you want to connect to");
            String server = Console.ReadLine();
            Console.WriteLine("Enter database name you want to connect to");
            String db = Console.ReadLine();
            Console.WriteLine("Enter username");
            String user = Console.ReadLine();
            Console.WriteLine("Enter password");
            String pw = Console.ReadLine();
            String connetionString = "Server=" + server + ";Database=" + db +"; User Id = " + user +"; Password = " + pw + ";";
            SqlConnection connection = new SqlConnection(connetionString);
            if (connection != null)
            {
                Console.WriteLine("Connected successfully");
            } else
            {
                Console.WriteLine("Connection failed");
                return;
            }
            String commandString = "";
            Console.WriteLine("Enter command you want to execute");

            try
            {
                connection.Open();
                while(true)
                {
                    commandString = Console.ReadLine();
                    if (commandString == "quit") return;
                    SqlCommand command = new SqlCommand(commandString, connection);
                    int t = command.ExecuteNonQuery();
                    command.Dispose();
                    Console.WriteLine("Affected " + t + "rows");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error happened\n" + e.Message);
            }
            finally
            {
                connection.Close();
            }
          
        }
    }
}





 
