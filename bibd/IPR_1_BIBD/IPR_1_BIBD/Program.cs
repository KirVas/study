﻿using System;
using System.Xml;

namespace ReadingXML2
{
    class Class1
    {
        static void createFile()
        {
            // Create a new file in C:\\ dir
            XmlTextWriter textWriter = new XmlTextWriter("C:\\usr\\myXmFile.xml", null);
            // Opens the document
            textWriter.WriteStartDocument();
            // Write comments
            textWriter.WriteComment("First Comment XmlTextWriter Sample Example");
            textWriter.WriteComment("myXmlFile.xml in root dir");
            // Write first element
            textWriter.WriteStartElement("student");
            textWriter.WriteStartElement("r");
            // Write next element
            textWriter.WriteStartElement("name", "");
            textWriter.WriteString("Student");
            textWriter.WriteEndElement();
            // Write one more element
            textWriter.WriteStartElement("address", ""); textWriter.WriteString("colony");
            textWriter.WriteEndElement();

            // WriteChars
            char[] ch = new char[3];
            ch[0] = 'a';
            ch[1] = 'r';
            ch[2] = 'c';
            textWriter.WriteStartElement("char");
            textWriter.WriteChars(ch, 0, ch.Length);
            textWriter.WriteEndElement();
            // Ends the document. 
            textWriter.WriteEndDocument();
            // close writer
            textWriter.Close();
        }

        static void removeAttribute(String path)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                Console.WriteLine("Enter node XPath");
                String nodepath = Console.ReadLine();
                Console.WriteLine();
                XmlNode node = null;
                try
                {
                    node = doc.SelectSingleNode(nodepath);
                    if (node == null) throw new Exception();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Wrong path");
                    return;
                }
                Console.WriteLine("Enter attribute name");
                String name = Console.ReadLine();
                Console.WriteLine();
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    if (attribute.Name.Equals(name))
                    {
                        node.Attributes.Remove(attribute);
                        break;
                    }
                }
                doc.Save(path);
                Console.WriteLine("Operation successful!");
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong path or another error");
            }
        }
        static void addAttribute(String path)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                Console.WriteLine("Enter node XPath");
                String nodepath = Console.ReadLine();
                Console.WriteLine();
                XmlNode node = null;
                try
                {
                    node = doc.SelectSingleNode(nodepath);
                    if (node == null) throw new Exception();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Wrong path");
                    return;
                }
                Console.WriteLine("Enter attribute name");
                String name = Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine("Enter attribute value");
                String value = Console.ReadLine();
                Console.WriteLine();

                XmlAttribute attribute = doc.CreateAttribute(name);
                attribute.Value = value;
                node.Attributes.Append(attribute);
                doc.Save(path);
                Console.WriteLine("Operation successful!");
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong path or another error");
            }

        }
        static void editAttribute(String path)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                Console.WriteLine("Enter node XPath");
                String nodepath = Console.ReadLine();
                Console.WriteLine();
                XmlNode node = null;
                try
                {
                    node = doc.SelectSingleNode(nodepath);
                    if (node == null) throw new Exception();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Wrong path");
                    return;
                }
                Console.WriteLine("Enter attribute name");
                String name = Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine("Enter attribute value");
                String value = Console.ReadLine();
                Console.WriteLine();
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    if (attribute.Name.Equals(name))
                    {
                        attribute.Value = value;
                        node.Attributes.Append(attribute);
                    }
                }
                doc.Save(path);
                Console.WriteLine("Operation successful!");
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong path or another error");
            }
        }
        static int ask(String path)
        {
            Console.WriteLine("Which action should be done?");
            Console.WriteLine("Type 'a' to add attribute");
            Console.WriteLine("Type 'e' to edit attribute");
            Console.WriteLine("Type 'r' to remove attribute");
            Console.WriteLine("Type 'q' to exit");

            int mode = Console.ReadKey().KeyChar;
            Console.WriteLine();
            switch (mode)
            {
                case 'e': editAttribute(path); break;
                case 'a': addAttribute(path); break;
                case 'r': removeAttribute(path); break;
                case 'q': return -1;
                default: break;
            }
            return 0;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Which file would you like to edit?");
            String path = Console.ReadLine();



            int repeat = 0;
            do
            {
                repeat = ask(path);
            } while (repeat == 0);
        }
    }
}