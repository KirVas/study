function z=funl32(x,y,cx)
sum=0;
pro=1;
length=numel(x)-1;
for i=0:length
    for j=0:length
        if i~=j
            pro=pro*((cx-x(j+1))/(x(i+1)-x(j+1)));
        end
    end
    sum=sum+y(i+1)*pro;
    pro=1;
end
z=sum;
return
