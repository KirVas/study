clear;
clc;
[x1,y1]=meshgrid(-10:0.5:10,-10:0.5:10);
z1=funl12(x1,y1);
mesh(x1,y1,z1)
hold on
[x2,y2]=meshgrid(-10:0.5:10,-10:0.5:10);
z2=funl13(x2,y2);
meshc(x2,y2,z2)
grid on
