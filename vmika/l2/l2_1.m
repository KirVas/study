clear;
clc;

a=[4,-5,-5; -3,-3,4; -5,2,-5];
a1=[4,-5,-5; -3,-3,4; -5,2,-5];
b=[5,10,-9];
b1=[5,10,-9];
n=3;

for i=1:n-1
    for j=i+1:n
        m=a(j,i)/a(i,i);
        a(j,i)=0;
        for k=i+1:n
            a(j,k)=a(j,k)-a(i,k)*m;
        end
        b(j)=b(j)-b(i)*m;
    end
end

x(n)=b(n)/a(n,n);

for i=n-1:-1:1
    t=0;
    for j=i+1:n
        t=t+a(i,j)*x(j);
    end
    x(i)=(b(i)-t)/a(i,i);
end

disp("Gauss");
disp(x');
disp("MATLAB");
disp(inv(a1)*b1');