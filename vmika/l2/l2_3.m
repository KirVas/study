clear;
clc;

a=[4,-5,-5; -3,-3,4; -5,2,-5];
b=[5,10,-9];
x=[0,0,0];

eps=0.00001;
n=3;
for iter=1:200
    max=0;
    for i=1:n
        sum=0;
        for j=1:i-1
            sum=sum+a(i,j)*x(j);
        end
        for j=i+1:n
            sum=sum+a(i,j)*x(j);
        end
        t=(b(i)-sum)/a(i,i);
        if abs(t-x(i))>max
            max=abs(t-x(i));
        end
        x(i)=t;
    end
    if max < eps
        break;
    end
end


disp("Gauss-Zeidel");
disp(x');
disp(iter);
disp("MATLAB");
disp(inv(a)*b');