clear;
clc;

a=[4,-5,-5; -3,-3,4; -5,2,-5];
a1=[4,-5,-5; -3,-3,4; -5,2,-5];
b=[5,10,-9];
b1=[5,10,-9];
n=3;

for i=1:n-1
    max=abs(a(i,i));
    imax=i;
    for t=i+1:n
        if abs(a(t,i))>max
            max=abs(a(t,i));
            imax=t;
        end
        if imax~=i
            for j=i:n
                p=a(i,j);
                a(i,j)=a(imax,j);
                a(imax,j)=p;
            end
            p=b(i);
            b(i)=b(imax);
            b(imax)=p;
        end
    end
    
    for j=i+1:n
        m=a(j,i)/a(i,i);
        a(j,i)=0;
        for k=i+1:n
            a(j,k)=a(j,k)-a(i,k)*m;
        end
        b(j)=b(j)-b(i)*m;
    end
end

x(n)=b(n)/a(n,n);

for i=n-1:-1:1
    t=0;
    for j=i+1:n
        t=t+a(i,j)*x(j);
    end
    x(i)=(b(i)-t)/a(i,i);
end

disp("Gauss");
disp(x');
disp("MATLAB");
disp(inv(a1)*b1');