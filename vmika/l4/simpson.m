function z=simpson(x,y)
h=x(2)-x(1);
n=numel(x);
z=0;
for i=1:(n-1)/2
    z=z+(h/3)*(y(2*i-1)+4*y(2*i)+y(2*i+1));
end
return