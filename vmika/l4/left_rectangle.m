function z=left_rectangle(x,y)
h=x(2)-x(1);
n=numel(x);
z=0;
for i=1:n-1
    z=z+h*y(i);
end
return