function [z1,z2]=gauss(func,a,b)
f=fcnchk(func);
t=[-0.774597,0,0.774597];
c=[5/9,8/9,5/9];
n=numel(t);
z1=0;
for i=1:n
    z1=z1+feval(f,(b-a)/2*t(i)+(a+b)/2)*c(i);
end
z1=z1*(b-a)/2;
t=[0.339981,-0.339981,0.861136,-0.861136];
c=[0.652145,0.652145,0.347855,0.347855];
n=numel(t);
z2=0;
for i=1:n
    z2=z2+feval(f,(b-a)/2*t(i)+(a+b)/2)*c(i);
end
z2=z2*(b-a)/2;
return