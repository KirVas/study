function res=trapeze(x,y)
h=x(2)-x(1);
n=numel(x);
res=0;
for i=1:n-1
    res=res+(h/2)*(y(i)+y(i+1));
end
return