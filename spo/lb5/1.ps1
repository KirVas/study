﻿# пример проводника

# $OutputEncoding = [ System.Text.Encoding]::UTF8
# не работает, для вывода в UTF8 необходимо сразу создавать файл с UTF8 началом ( UTF-8 BOM)

$files = ls | % { $_ }
Write-Output $files.Length
return

    $Host.UI.RawUI.FlushInputBuffer()   # Make sure buffered input doesn't "press a key" and skip the ReadKey().
    $L_KEY = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp")
    $L_KEY = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp")
   	Write-Output $L_KEY.ToString()
   	Write-Output VirtualKeyCode $L_KEY.VirtualKeyCode.
return


$L_DIR = (Get-Item -Path ".\").FullName
Write-Output $L_DIR

$_sel=" •"

$files.ToString()
foreach ($file in $files)
{
		Write-Output ( "║ {0,2} {1,-20} {2,1} {3,16} {4,-32} ║" -f $_sel, $file.Name.PadRight(20).Substring(0,20), $file.Mode.Substring(0,1), $file.LastWriteTime.ToString("yyyy/mm/dd hh:mm"), $file.Length.ToString().PadRight(32).Substring(0,32) )
}
return

║  • batch_fileman.cmd    - 2020/51/11 09:51 3973                             ║
║  • batch_fileman.vbs    - 2020/51/11 09:51 2728                             ║
║  • env.cmd              - 2020/40/12 04:40 461                              ║
║  • fun.cmd              - 2020/51/11 09:51 7611                             ║
║  • sample.vbs           - 2020/51/1

╔═════════════════════════════════════════════════════════════════════════════╗
║Path: C:\bin\cmd\Samples\Kir\0                                               ║
║                                                                             ║
║ •» ps1.smpl             d 12.05.2020 10:57 0                                ║
║  • samples              d 11.05.2020 09:51 4096                             ║
║  • .todo                - 12.05.2020 16:39 4401                             ║
║  • 1.ps1                - 12.05.2020 16:40 6364                             ║
║  • batch_fileman.cmd    - 11.05.2020 09:51 3973                             ║
║  • batch_fileman.vbs    - 11.05.2020 09:51 2728                             ║
║  • env.cmd              - 12.05.2020 16:40 237                              ║
║  • fun.cmd              - 11.05.2020 09:51 7611                             ║
║  • sample.vbs           - 11.05.2020 09:51 9152                             ║
║  • sample2.vbs          - 11.05.2020 09:51 1497                             ║
║                                                                             ║
║                                                                             ║
║                                                                             ║
║                                                                             ║
╟──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬───────────╢
║ q - exit │ w - prev │ s - next │ a - pgup │ d - pgdn │ ? - help │ : - run   ║
╚══════════╧══════════╧══════════╧══════════╧══════════╧══════════╧═══════════╝
#               4 Dir(s)  110 589 825 024 bytes free
#              10 File(s)         44 566 bytes
#•» ps1.smpl  12.05.2020 10:57 d---------- 0
Enter command:



Write-Output ( “The {0} thinks that {1}!” -f $file.Name, $file.Mode )
# "Hello world" | Get-Member
return


# $file | Get-Member

Write-Output $file.LinkType
Write-Output $file.Mode
Write-Output $file.Target
Write-Output $file.AppendText
#Write-Output $file.CopyTo
#Write-Output $file.Create
#CreateObjRef
#CreateText
#Decrypt
#Delete
#Encrypt
#Equals
Write-Output $file.GetAccessControl
#GetHashCode
#GetLifetimeService
#GetObjectData
Write-Output $file.GetType
#InitializeLifetimeServi
#MoveTo
#Open
#OpenRead
#OpenText
#OpenWrite
#Refresh
#Replace
#SetAccessControl
#ToString
Write-Output $file.PSChildName
Write-Output $file.PSDrive
Write-Output $file.PSIsContainer
Write-Output $file.PSParentPath
Write-Output $file.PSPath
Write-Output $file.PSProvider
Write-Output $file.Attributes
Write-Output $file.CreationTime
Write-Output $file.CreationTimeUtc
Write-Output $file.Directory
Write-Output $file.DirectoryName
Write-Output $file.Exists
Write-Output $file.Extension
Write-Output $file.FullName
Write-Output $file.IsReadOnly
#LastAccessTime
#LastAccessTimeUtc
#LastWriteTime
#LastWriteTimeUtc
Write-Output $file.Length
Write-Host  "Name										$file.Name"
Write-Host  "BaseName 							$file.BaseName"
#Write-Output $file.VersionInfo

return

[String]$L_TEXT = ""

# функции пишутся перед их использованием
Function Quit($Text) {
    Write-Host $L_TEXT $Text
    Break Script
}

Clear-Host




    $Host.UI.RawUI.FlushInputBuffer()   # Make sure buffered input doesn't "press a key" and skip the ReadKey().
    $L_KEY = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
   	Write-Output $L_KEY.ToString()
   	Write-Output VirtualKeyCode $L_KEY.VirtualKeyCode.

$L_TEXT = "Quiting because: "
Quit("BYE")
Return


# 1) закрывает и шелл      [Environment]::Exit(1)
# 2) вылет с ошибкой       throw "Error Message"
# 3) вылет из конструкции  Break

Function TestBreak() {
  $Breakout=$true
#  $Breakout=$false

  If ($Breakout -eq $true)
  {
       Write-Host "Break Out!"
       $Breakout=$false
#       Return  # вылетел из функции
#       Break   # вобще вылетел из скрипта
  }
  ElseIf ($Breakout -eq $false)
  {
       Write-Host "No Breakout for you!"
  }
  Else
  {
      Write-Host "Breakout wasn't defined..."
  }

  Write-Host "End of break test"
}

TestBreak

# Существует также интересная особенность, заключающаяся в том, что Break
# вы можете добавлять префикс цикла к метке, а затем вы можете выйти из этого помеченного цикла,
# даже если Breakкоманда вызывается внутри нескольких вложенных групп в этом помеченном цикле.

While ($true) {
    Break   # а из цикла вырываемся по честному, скрипт продолжается с места за циклом
    # Code here will run
    :myLabel While ($true) {
        # Code here will run
        While ($true) {
            # Code here will run
            While ($true) {
                # Code here will run
                Break myLabel
                # Code here will not run
            }
            # Code here will not run
        }
        # Code here will not run
    }
    # Code here will run
}



# If running in the console, wait for input before closing.
if ($Host.Name -eq "ConsoleHost")
{
		Write-Output "╔═════════════════════════════════════════════════════════════════════════════╗"
		Write-Output "║  Here is hepl for this batch routines                                       ║"
		Write-Output "║                                                                             ║"
		Write-Output "║  some text...                                                               ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "╟─────────────────────────────────────────────────────────────────────────────╢"
		Write-Output "║ Press any key for continue                                                  ║"
		Write-Output "╚═════════════════════════════════════════════════════════════════════════════╝"

    Write-Host "Press any key to continue..."
    $Host.UI.RawUI.FlushInputBuffer()   # Make sure buffered input doesn't "press a key" and skip the ReadKey().
    $L_KEY = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}
