' ���� ������ ����������, ��� ������������ ������ WSHShell 
' ��� �������� ������ �� ������� �����.

L_Welcome_MsgBox_Message_Text   = _
    "���� ������ ������� ����� ��� Notepad �� ������� �����."
L_Welcome_MsgBox_Title_Text     = "������ Windows Scripting Host "
Call Welcome()

' *************************************************************** *
' * ������, ��������� � ��������.
' *DimWSHShell
Set WSHShell = WScript.CreateObject("WScript.Shell")

Dim MyShortcut, MyDesktop, DesktopPath

' ������ ���� � �������� �����
DesktopPath = WSHShell.SpecialFolders("Desktop")

' ������� ����� �� ������� �����
Set MyShortcut = WSHShell.CreateShortcut(DesktopPath & _
    "\Shortcut to notepad.lnk")

' ������ �������� �������-������ � ��������� ��
MyShortcut.TargetPath = WSHShell.ExpandEnvironmentStrings _
    ("%windir%\notepad.exe")
MyShortcut.WorkingDirectory = WSHShell.ExpandEnvironmentStrings _
    ("%windir%")
MyShortcut.WindowStyle = 4
MyShortcut.IconLocation = WSHShell.ExpandEnvironmentStrings _
    ("%windir%\notepad.exe, 0")
MyShortcut.Save

WScript.Echo "������ �� ������� ����� ���� ����� ��� Notepad."

' ****************************************************************
' *
' * ����� ����������
' *
Sub Welcome()
    Dim intDoIt

    intDoIt =  MsgBox(L_Welcome_MsgBox_Message_Text, _
        vbOKCancel + vbInformation, _
        L_Welcome_MsgBox_Title_Text )
    If intDoIt = vbCancel Then
       WScript.Quit
    End If
End Sub

