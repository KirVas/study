﻿# File explorer

$Host.UI.RawUI.FlushInputBuffer()

## initial cd
[String]$global:L_CD = (Get-Item -Path ".\").FullName
## default sort order
[String]$global:L_DEF_DIR_ORDER = "/o"
## visible list length
[Int]$global:L_CFG_HEIGHT = 20
## initial first vicible file
[Int]$global:L_TOPFILE = 1

[Int]$global:L_CURLINE = 1
[String]$global:L_FILE = ""
[String]$global:L_FILEATTR = ""
[String]$global:L_FILEEXT = ""
[String]$global:L_FILEPATH = ""
[Int]$global:L_FILESCOUNT = 0
[Int]$global:L_FILESIZE = 0
[String]$global:L_FILETIME = ""
[String]$global:L_FULLPATH = ""




##############################################################################################################
# Functions section
##############################################################################################################
Function Quit($Text) {
  Write-Host $Text
  Break Script
}

Function Pause($object) {
  Write-Output $object
	$Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp")
	$Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp")
}



Function SetCurrent($file) {
	$global:L_FILE				= $file.Name
	$global:L_FILEATTR		= $file.Mode
	$global:L_FILEEXT		  = $file.Extension
	$global:L_FILEPATH    = $file.DirectoryName
	$global:L_FILESIZE		= $file.Length
	$global:L_FILETIME    = $file.LastWriteTime.ToString("yyyy/mm/dd hh:mm")
	$global:L_FULLPATH    = $file.FullName
}

Function Refresh() {
  Clear-Host

  $line = 0
  $files = ls | % { $_ }
  $global:L_FILESCOUNT = $files.Length

	Write-Output "╔═════════════════════════════════════════════════════════════════════════════╗"
	Write-Output ( "║Path: {0,70} ║" -f $global:L_CD.PadRight(71).Substring(0,70) )
	Write-Output "║                                                                             ║"
  [Int]$i = 0

  ForEach ($file in $files) {
		$i = $i + 1
		if ( $i -lt $global:L_TOPFILE ) {
			# NOP
		} else {
  		$line = $line + 1
  		if ( $global:L_CURLINE -eq $line ) {
  			$_sel = "•»"
  			SetCurrent($file)
  		} else {
  			$_sel = " •"
  		}

  		Write-Output ( "║ {0,2} {1,-20} {2,1} {3,16} {4,-32} ║" -f $_sel, $file.Name.PadRight(20).Substring(0,20), $file.Mode.Substring(0,1), $file.LastWriteTime.ToString("yyyy/mm/dd hh:mm"), $file.Length.ToString().PadRight(32).Substring(0,32) )
  		if ( $line -ge $global:L_CFG_HEIGHT ) {
#  			Write-Output "Ooops"  ###############################
  			Break
  		}
		}
  }
	# fill bottom
	While ( $line -lt $global:L_CFG_HEIGHT ) {
		$line = $line + 1
		Write-Output "║                                                                             ║"
  }

	Write-Output "╟──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬───────────╢"
	Write-Output "║ q - exit │ Up  prev │ Dn  next │ F7 mkdir │ F8 rmdir │ F1  help │ / - run   ║"
	Write-Output "╚══════════╧══════════╧══════════╧══════════╧══════════╧══════════╧═══════════╝"
	Write-Output ( "{0,40:n0} bytes free" -f $file.PSDrive.Free )
	Write-Output ( "•» {0} {1} {2} {3}" -f $global:L_FILE, $global:L_FILETIME, $global:L_FILEATTR, $global:L_FILESIZE.ToString() )

  Return
}

Function Help() {
  Clear-Host
	Write-Output "╔═════════════════════════════════════════════════════════════════════════════╗"
	Write-Output "║  Here is hepl for this batch routines                                       ║"
	Write-Output "║                                                                             ║"
	Write-Output "║  some text...                                                               ║"
	Write-Output "║                                                                             ║"
	Write-Output "║                                                                             ║"
	Write-Output "║                                                                             ║"
	Write-Output "║                                                                             ║"
	Write-Output "║                                                                             ║"
	Write-Output "║                                                                             ║"
	Write-Output "║                                                                             ║"
	Write-Output "╟─────────────────────────────────────────────────────────────────────────────╢"
	Write-Output "║ Press any key for continue                                                  ║"
	Write-Output "╚═════════════════════════════════════════════════════════════════════════════╝"

  Write-Host "Press any key to continue..."
  $Host.UI.RawUI.FlushInputBuffer()
  $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")

  Return
}

##############################################################################################################
Function Main() {
  $Host.UI.RawUI.FlushInputBuffer()
  While ($true) {
    Refresh

    $Host.UI.RawUI.FlushInputBuffer()
    $L_KEY = $null
    $L_KEY = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp")  #IncludeKeyDown
    $HOST.UI.RawUI.Flushinputbuffer()

    switch($L_KEY.VirtualKeyCode) {
      13 { # Enter      - select operation
       	if ( $global:L_FILEATTR[0] -eq "d"  ) {
       	  chdir $global:L_FILE
  				$global:L_TOPFILE = 1
  				$global:L_CURLINE = 1
					$global:L_CD = (Get-Item -Path ".\").FullName
       	} else {
          Try {
          	$_cmd = $global:L_FULLPATH
          	Start-Process -FilePath $_cmd -Wait -ErrorAction Stop
          }
          Catch  {
            $Result = [System.Environment]::Exitcode
          }
       	}
      }
      118 { # F7        - create folder
          Try {
            Refresh
           	$_new_dir = Read-Host -Prompt "Enter folder name: "
    				$Host.UI.RawUI.FlushInputBuffer()
           	mkdir $_new_dir
          }
          Catch  {
            $Result = [System.Environment]::Exitcode
          }
      }
      119 { # F8        - delete folder
         	if ( $global:L_FILEATTR[0] -eq "d"  ) {
            Try {
              Refresh
             	$_answer = Read-Host -Prompt "Are you sure you want to delete the directory? [n/y]: "
    					$Host.UI.RawUI.FlushInputBuffer()
             	if ( $_answer -eq "y" ) {
             		rmdir $global:L_FULLPATH
             	}
            }
            Catch  {
              $Result = [System.Environment]::Exitcode
            }
          }
      }
      38 { # Up         - toPrevLine
       	if ( $global:L_CURLINE -gt 1 ) {
       		$global:L_CURLINE = $global:L_CURLINE - 1
       	}
      }
      40 { # Down       - toNextLine
       	if ( $global:L_CURLINE -lt $L_CFG_HEIGHT ) {
       		$global:L_CURLINE = $global:L_CURLINE + 1
       	}
      }
      33 { # PgUp       - toPrevPage
        $global:L_TOPFILE = $global:L_TOPFILE - $global:L_CFG_HEIGHT + 1
      	if ( $global:L_TOPFILE -ge $global:L_FILESCOUNT ) {
      	  $global:L_TOPFILE = $global:L_FILESCOUNT - 1
      	}
      	if ( $global:L_TOPFILE -lt 1 ) {
      	  $global:L_TOPFILE = 1
      	}
      }
      34 { # PgDn       - toNextPage
        $global:L_TOPFILE = $global:L_TOPFILE + $global:L_CFG_HEIGHT - 1
      	if ( $global:L_TOPFILE -ge $global:L_FILESCOUNT ) {
      	  $global:L_TOPFILE = $global:L_FILESCOUNT - 1
      	}
      	if ( $global:L_TOPFILE -lt 1 ) {
      	  $global:L_TOPFILE = 1
      	}
      }
      36 { # Home        - toHome
				$global:L_TOPFILE = 1
				$global:L_CURLINE = 1
      }
      35 { # End         - toEndList
				$global:L_TOPFILE = $global:L_FILESCOUNT
				$global:L_CURLINE = 1
      }
      191 { # Slash  - runCommand
        Try {
          Refresh
         	$_command = Read-Host -Prompt "Enter command: "
    			$Host.UI.RawUI.FlushInputBuffer()
         	if ( $_command -ne "" ) {
#            	Start-Process $_command -Wait -ErrorAction Stop
						powershell -command $_command
						Write-Output "Press enter for continue..."
						Read-Host
    				$HOST.UI.RawUI.Flushinputbuffer()

         	}
        }
        Catch  {
          $Result = [System.Environment]::Exitcode
        }
      }
      220 { # Backslash  - toRoot
       	chdir \
       	$global:L_CD = (Get-Item -Path ".\").FullName
      }
      8 { # Backspace   - toParent
       	chdir ".."
       	$global:L_CD = (Get-Item -Path ".\").FullName
      }
      { $_ -eq 72 -or $_ -eq 112 } { # Help
       	Help
      }
      82 { # Refresh
				$global:L_TOPFILE = 1
				$global:L_CURLINE = 1
				$global:L_CD = (Get-Item -Path ".\").FullName
      }
      { $_ -eq 81 -or $_ -eq 88 } { # q/x - quit
       	Quit("BYE")
      }
      Default {
      	Write-Output $L_KEY.ToString()
      }
    }
  }
}

Main
