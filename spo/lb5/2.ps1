﻿# пример проводника

# $OutputEncoding = [ System.Text.Encoding]::UTF8
# не работает, для вывода в UTF8 необходимо сразу создавать файл с UTF8 началом ( UTF-8 BOM)

$files = ls | % { $_ }

foreach ($file in $files)
{
		Write-Output ( "║ {0,2} {1,-20} {2,1} {3,16} {4,-32} ║" -f $_sel, $file.Name.PadRight(20).Substring(0,20), $file.Mode.Substring(0,1), $file.LastWriteTime.ToString("yyyy/mm/dd hh:mm"), $file.Length.ToString().PadRight(32).Substring(0,32) )
}

# .Root
#               4 Dir(s)  110 589 825 024 bytes free
#              10 File(s)         44 566 bytes
#•» ps1.smpl  12.05.2020 10:57 d---------- 0
		Write-Output ( "{0,40:n0} bytes free" -f $file.PSDrive.Free )
		Write-Output ( "{0,16} File(s) {1,20:n0} bytes" -f $files.Length, $file.PSDrive.Used )

		Write-Output $file.Mode[0].CompareTo('-')
#		Write-Output $file.Mode[0] | Get-Member
		
		

#Write-Output $file.PSDrive.Free

# | Get-Member

return

# $file | Get-Member

Write-Output $file.LinkType
Write-Output $file.Mode
Write-Output $file.Target
Write-Output $file.AppendText
#Write-Output $file.CopyTo
#Write-Output $file.Create
#CreateObjRef
#CreateText
#Decrypt
#Delete
#Encrypt
#Equals
Write-Output $file.GetAccessControl

#GetHashCode
#GetLifetimeService
#GetObjectData
Write-Output $file.GetType
#InitializeLifetimeServi
#MoveTo
#Open
#OpenRead
#OpenText
#OpenWrite
#Refresh
#Replace
#SetAccessControl
#ToString
Write-Output $file.PSChildName

Write-Output "++++++++++"
Write-Output $file.PSDrive.Root
return
Write-Output $file.PSIsContainer
Write-Output $file.PSParentPath
Write-Output $file.PSPath
Write-Output $file.PSProvider
Write-Output $file.Attributes
Write-Output $file.CreationTime
Write-Output $file.CreationTimeUtc
Write-Output $file.Directory
Write-Output $file.DirectoryName
Write-Output $file.Exists
Write-Output $file.Extension
Write-Output $file.FullName
Write-Output $file.IsReadOnly
#LastAccessTime
#LastAccessTimeUtc
#LastWriteTime
#LastWriteTimeUtc
Write-Output $file.Length
Write-Host  "Name										$file.Name"
Write-Host  "BaseName 							$file.BaseName"
#Write-Output $file.VersionInfo

return
