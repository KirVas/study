﻿# пример проводника

# $OutputEncoding = [ System.Text.Encoding]::UTF8
# не работает, для вывода в UTF8 необходимо сразу создавать файл с UTF8 началом ( UTF-8 BOM)

Clear-Host

# If running in the console, wait for input before closing.
if ($Host.Name -eq "ConsoleHost")
{
		Write-Output "╔═════════════════════════════════════════════════════════════════════════════╗"
		Write-Output "║  Here is hepl for this batch routines                                       ║"
		Write-Output "║                                                                             ║"
		Write-Output "║  some text...                                                               ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "║                                                                             ║"
		Write-Output "╟─────────────────────────────────────────────────────────────────────────────╢"
		Write-Output "║ Press any key for continue                                                  ║"
		Write-Output "╚═════════════════════════════════════════════════════════════════════════════╝"

    Write-Host "Press any key to continue..."
    $Host.UI.RawUI.FlushInputBuffer()   # Make sure buffered input doesn't "press a key" and skip the ReadKey().
    $L_KEY = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}
