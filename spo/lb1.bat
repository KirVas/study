@echo off & @setlocal ENABLEEXTENSIONS & @setlocal ENABLEDELAYEDEXPANSION >nul 2>&1

  if ".%1" equ "." (
    echo Transliterate utility
    echo Syntax:
    echo    %~n0 ^<cyrillic text^>
    echo Result of transliterate output to result.txt
    exit /b
  ) 
  set word=%*
  set word=%word:�=a%
  set word=%word:�=b%
  set word=%word:�=v%
  set word=%word:�=g% 
  set word=%word:�=d%
  set word=%word:�=e%
  set word=%word:�=yo% 
  set word=%word:�=zh% 
  set word=%word:�=z% 
  set word=%word:�=i% 
  set word=%word:�=y% 
  set word=%word:�=k% 
  set word=%word:�=l% 
  set word=%word:�=m% 
  set word=%word:�=n% 
  set word=%word:�=o% 
  set word=%word:�=p% 
  set word=%word:�=r% 
  set word=%word:�=s% 
  set word=%word:�=t% 
  set word=%word:�=u% 
  set word=%word:�=f% 
  set word=%word:�=h%
  set word=%word:�=c% 
  set word=%word:�=ch% 
  set word=%word:�=sh% 
  set word=%word:�=sch% 
  set word=%word:�='% 
  set word=%word:�=y% 
  set word=%word:�='% 
  set word=%word:�=e% 
  set word=%word:�=yu% 
  set word=%word:�=ya%

	echo %word%
