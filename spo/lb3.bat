@echo off & @setlocal ENABLEEXTENSIONS & @setlocal ENABLEDELAYEDEXPANSION & chcp 65001 >nul 2>&1

  if not ".%2"=="." if /I not "%1"=="--help" if /I not "%1"=="-h" if /I not "%1"=="/h" if /I not "%1"=="-?" if /I not "%1"=="/?" goto start:
:help
  echo Network info
  echo Syntax:
  echo    %~n0  ^<remote url^> ^<mtu calculation host^> 
  echo For example: %~n0 www.opera.com 192.168.1.86
  exit /b
:start

  set p_remote_url=%1
  set p_test_point=%2

  set _log=output.txt
  set _def_gateway=

  ( echo Report for the lab 3 && echo %date%-%time% && echo ====================== && echo. ) > %_log%

:: wmic nicconfig list instance
  for /f "usebackq skip=1 tokens=1* delims= " %%i in (`wmic nic where NetEnabled^=true list instance ^| findstr ^/R "."`) do if ".%%i" neq "." if /I ".%%i" neq ".index" (
  	echo Instance %%i          			>> %_log%
  	echo ==============        			>> %_log%
  	call :get_interfaces_info	 %%i 	>> %_log%
  	echo ..............        			>> %_log%
  	echo.>> %_log%
  )      

  if ".%_def_gateway%" neq "." (
  	set /a _err=1
    for /f "usebackq tokens=1* delims=" %%i in (`ping %_def_gateway% -n 1 ^| findstr "(100%% loss^)"`) do set /a _err=0
    if !_err! equ 0 set _def_gateway=
  )

  call :get_net_info >> %_log%

exit /b

:: ------------------------------------------------------------------------------
:: interface info
:: ------------------------------------------------------------------------------
:get_interfaces_info
  if ".%1" equ "." exit /b 0

rem Информацию об IP-адресе сетевого адаптера, маске сети и шлюзе по умолчанию.
  echo.
  echo POINT 1
  echo -------
  for /f "usebackq tokens=1,2* delims==" %%i in (`wmic nicconfig WHERE Index^=%1 GET Description^,IPAddress^,IPSubnet^,DNSServerSearchOrder^,DefaultIPGateway ^/VALUE ^| findstr ^/R "."`) do if ".%%i" neq "." (
    call set _value="%%~j"
  	if /i ".!_value!" neq "." call :format_value %%i !_value!
    call set L_%%i=!_value!
  )
	
rem fix the wmic empty lineend (LF)
	call set L_DefaultIPGateway=%L_DefaultIPGateway%

	if not defined _def_gateway if defined L_DefaultIPGateway (
	  set _def_gateway=%L_DefaultIPGateway:~2,-2%
	)

rem Информацию о физическом адресе сетевой платы.
  echo.
  echo POINT 2
  echo -------
  for /f "usebackq tokens=1,2* delims==" %%i in (`wmic nicconfig WHERE Index^=%1 GET MACAddress ^/VALUE ^| findstr ^/R "."`) do if ".%%i" neq "." (
    call set _value="%%~j"
  	if /i ".!_value!" neq "." call :format_value %%i !_value!
  )
rem Информацию о доступности серверов DHCP, DNS и удаленного узла (в соответствии с вариантом).
  echo.
  echo POINT 3
  echo -------
  for /f "usebackq tokens=1,2* delims==" %%i in (`wmic nicconfig WHERE Index^=%1 GET DNSServerSearchOrder^,DHCPServer^,FullDNSRegistrationEnabled ^/VALUE ^| findstr ^/R "."`) do if ".%%i" neq "." (
    call set _value="%%~j"
  	if /i ".!_value!" neq "." call :format_value %%i !_value!
  )

rem Информацию о пропускной способности сети.
	echo.
	echo POINT 5
	echo -------
	for /f "usebackq tokens=1* delims==" %%i in (`wmic nic where Index^=%1 get Speed ^/VALUE ^| findstr ^/R "."`) do call :format_speed %%j
	
	call :format_value "Network bandwidth:" %_speed%

	exit /b 0
:end_ get_interfaces_info

:: ------------------------------------------------------------------------------
:: human-readable interface speed format
:: ------------------------------------------------------------------------------
:format_speed in _speed
  set _speed=%1
  set _speedKb=%_speed:~,-3%
  if ".%_speedKb%" equ "." set _speed=%1b && exit /b
  set _speedMb=%_speed:~,-6%
  if ".%_speedMb%" equ "." set _speed=%_speedKb%Kb && exit /b
  set _speedGb=%_speed:~,-9%
  if ".%_speedGb%" equ "." set _speed=%_speedMb%Mb && exit /b
  set _speedTb=%_speed:~,-12%
  if ".%_speedTb%" equ "." set _speed=%_speedGb%Gb && exit /b
  set _speed=%_speedTb%Tb
	exit /b 0
:end_ format_speed

:: ------------------------------------------------------------------------------
:: human-readable interface value
:: ------------------------------------------------------------------------------
:format_value in _pkey, _pvalue
	set _pkey=%~1                                                                                  .
	set _pvalue=%~2
	echo     %_pkey:~,40% %_pvalue%
	exit /b 0
:end_ format_value

:: ------------------------------------------------------------------------------
:: get common net info
:: ------------------------------------------------------------------------------
:get_net_info
rem Адреса первых трех маршрутизаторов, находящихся между компьютером и удаленным узлом
  echo.
	echo POINT 4
	echo -------
	if defined _def_gateway (
rem Информацию о доступности основного шлюза и доступности удаленного узла (в соответствии с вариантом).
	  echo CHECK ACCESSIBLE THE DEFAULT GATEWAY %_def_gateway% AND %p_remote_url%
	  echo METHOD: ping
		echo.
		echo Default gateway
    echo ---------------
		ping %_def_gateway%
		echo.
		echo Remote point
    echo ---------------
		ping %p_remote_url%
	) else (
	  echo NO DAFAULT GATEWAY
	)

	echo.
  echo POINT 6
  echo -------
	if defined _def_gateway (
  	set /a _i=0
  	for /f "usebackq skip=3 tokens=1* delims=" %%i in (`call pathping -4 -h 3 -w 1 -q 1 %p_remote_url%`) do (
  		if !_i! leq 4 (
    		echo %%i
    		set /a _i=!_i!+1
    	)
  	) 
	) else (
	  echo NO DAFAULT GATEWAY
	)

  rem Список роутеров, расположенных между компьютером и удаленным узлом
  echo.
  echo POINT 7
  echo -------
	if defined _def_gateway (
		tracert -4 -d -h 100 -w 1 %p_remote_url%
	) else (
	  echo NO DAFAULT GATEWAY
	)

rem Определить самые медленнные участки, расположенные на маршруте от компьютера до удаленного узла
  echo.
  echo POINT 8
  echo -------
	if defined _def_gateway (
		call pathping -4 -h 3 -w 1 -q 1 %p_remote_url%
	) else (
	  echo NO DAFAULT GATEWAY
	)

rem Таблицу ARP компьютера
  echo.
  echo POINT 9
  echo -------
  arp -a

rem Список текущих сетевых подключений
  echo.
  echo POINT 10
  echo -------
	netstat -f

rem Максимальный размер кадра канального уровня (MTU) в сети. Для
rem получения размера необходимо посылать пакеты различной длины при
rem установленном флаге запрета фрагментации. В качестве удаленного узла
rem можно использовать адрес любого соседнего компьютера.
  echo.
	echo POINT 11
	echo -------
	if defined _def_gateway (
	  call :get_mtu
	  set /a _mtu=0
		call :format_value "MTU ~ " "!_mtu!"
	) else (
	  echo NO DAFAULT GATEWAY
	)

	exit /b 0
:end_ get_net_info


:: ------------------------------------------------------------------------------
:: calculate mtu
:: ------------------------------------------------------------------------------
:get_mtu
:: 65500 - max of ping -l parameter
	if ".%_mtu%" equ "." set /a _mtu=65500
	set /a _old=0

:: max 20 steps for calculate
	set /a _counter=0

:while 
	set /a _counter= !_counter! + 1
  if !_counter! geq 20 goto stop_loop:

::  set _
::	pause

	set /a _err=1
  for /f "usebackq tokens=1* delims=" %%i in (`ping %p_test_point% -f -n 1 -l %_mtu% ^| findstr "(100%% loss^)"`) do set /a _err=0
  if %_err% equ 0 (
    set /a _delta= %_mtu% - %_old%
    set /a _delta = !_delta! / 2
    set /a _mtu = %_mtu% - !_delta!
  	goto while:
  ) else (
    set /a _delta = %_mtu% - %_old%
    set /a _delta = !_delta! / 2
    set /a _old = %_mtu%
    set /a _mtu = %_mtu% + !_delta!
  	goto while:
  )
goto while:
:stop_loop

	exit /b 0
:end_ get_mtu

exit


