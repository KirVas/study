Option Explicit

Dim strPath

strPath = SelectFolder()
If strPath = vbNull Then
    WScript.Echo "Cancelled"
Else
    CreateObject("WScript.Shell").Run strPath
End If


Function SelectFolder()

    Dim objFolder, objItem, objShell
    
    On Error Resume Next
    SelectFolder = vbNull

    Set objShell  = CreateObject( "Shell.Application" )
    Set objFolder = objShell.BrowseForFolder( 0, "Select Folder", &h4000, 0)

    If IsObject( objfolder ) Then SelectFolder = objFolder.Self.Path

    Set objFolder = Nothing
    Set objshell  = Nothing
    On Error Goto 0
End Function