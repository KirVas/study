@echo off & @setlocal ENABLEEXTENSIONS & @setlocal ENABLEDELAYEDEXPANSION & chcp 65001 >nul 2>&1

:: initial cd
set L_CD=%cd%
:: default sort order
set L_DEF_DIR_ORDER=/o
:: path to executable folder
set L_CMDPATH=%~dp0 && set L_CMDPATH=!L_CMDPATH:~0,-2!
:: visible list length
set /a L_CFG_HEIGHT=20
:: initial first vicible file
set L_TOPFILE=1

set L_CURLINE=1
set L_FILE=
set L_FILEATTR=
set L_FILEEXT=
set L_FILEPATH=
set L_FILESCOUNT=0
set L_FILESIZE=
set L_FILETIME=
set L_FULLPATH=
set L_KEY=


set m-=call %L_CMDPATH%\fun.cmd -
set env=call %L_CMDPATH%\env.cmd
set set_cd=for /F "usebackq tokens=1* delims=" %%i in (`cd`) do set L_CD=%%i

if exist %L_CMDPATH%\env.cmd %env%
%m-%save_env
%env%


:: test external call
:: %m-%fun1 param1
:: echo errorlevel %errorlevel%
::exit

:main_loop
	%m-%get_files_count
  %m-%refresh
  %env%
  set L_ERR=

  set /P L_KEY=Enter command: 
  %m-%save_env

::  echo command: %L_KEY%
::  %m-%fun1 param1
::  call %L_CMDPATH%\env.cmd

  if ".%L_KEY%" equ ".q" exit
:refresh
  if ".%L_KEY%" equ ".r" (
    set /a L_CURLINE=1
    set /a L_TOPFILE=1
    %set_cd%
    %m-%save_env
  	goto main_loop:
  )
  if ".%L_KEY%" equ ".?" (
    %m-%get_help
  	goto main_loop:
  )

  if ".%L_KEY%" equ ".d" (
rem select next page
    set /a L_TOPFILE=%L_TOPFILE%+%L_CFG_HEIGHT%-1
  	if !L_TOPFILE! geq %L_FILESCOUNT% set /a L_TOPFILE=%L_FILESCOUNT%-1
  	if !L_TOPFILE! lss 1 set /a L_TOPFILE=1
  ) else if ".%L_KEY%" equ ".a" (
rem select previous page
    set /a L_TOPFILE=%L_TOPFILE%-%L_CFG_HEIGHT%+1
  	if !L_TOPFILE! geq %L_FILESCOUNT% set /a L_TOPFILE=%L_FILESCOUNT%-1
  	if !L_TOPFILE! lss 1 set /a L_TOPFILE=1
  ) else if ".%L_KEY%" equ ".w" (
rem select previous file
    set /a L_CURLINE=%L_CURLINE%-1
  	if %L_CURLINE% lss 1 set /a L_CURLINE=1
  	if %L_CURLINE% gtr %L_CFG_HEIGHT% set /a L_CURLINE=%L_CFG_HEIGHT%
  ) else if ".%L_KEY%" equ ".s" (
rem select next file
    set /a L_CURLINE=%L_CURLINE%+1
  	if %L_CURLINE% lss 1 set /a L_CURLINE=1
  	if %L_CURLINE% gtr %L_CFG_HEIGHT% set /a L_CURLINE=%L_CFG_HEIGHT%

rem ----------------------------------------------------
rem file oiperations
rem ----------------------------------------------------
  ) else if ".%L_KEY:~0,3%" equ ".md " (
     for /F "usebackq tokens=1,2* delims= " %%i in (`echo %L_KEY%`) do ( set _DIR=%%j )
		 md %L_CD%\!_DIR! 2>%temp%\err.out
		 if !errorlevel! neq 0 set /P L_ERR=<%temp%\err.out
  ) else if ".%L_KEY%" equ ".rd" (
		if ".%L_FILEATTR:~0,1%" equ ".d" (
		 	set /P _ANSW=Are you sure you want to delete the directory? [n/y]:n
  		if /I ".!_ANSW!" equ ".y" (
    		echo rd /Q /S %L_CD%\%L_FILE% 2>%temp%\err.out
    		pause
    		rd /Q /S %L_CD%\%L_FILE% 2>%temp%\err.out
    		if !errorlevel! neq 0 set /P L_ERR=<%temp%\err.out
  		)
		) else (
			set L_ERR=The entry is not directory
		)
  ) else if ".%L_KEY:~0,3%" equ ".cp " (
		echo copy not released
  ) else if ".%L_KEY:~0,3%" equ ".rm " (
		echo delete not released
  ) else if ".%L_KEY:~0,3%" equ ".mv " (
		echo move not released
  ) else if ".%L_KEY%" equ "..." (
rem select parent directory
    set /a L_CURLINE=1
    set /a L_TOPFILE=1
		cd ..
		
  ) else if ".%L_KEY%" equ ".\" (
rem select root directory
    set /a L_CURLINE=1
    set /a L_TOPFILE=1
		cd \

  ) else if ".%L_KEY%" equ ".:" (
rem start current file
rem todo Или вынести в процедуру? можно корректно обработать строку
		if ".%L_FILEATTR%" neq "." if "%L_FILEATTR:~0,1%" equ "d" (
    	set /a L_CURLINE=1
    	set /a L_TOPFILE=1
			cd /d %L_CD%\%L_FILE%
			
		) else if ".%L_FILE%" neq "."  start "" "%L_FILE%"
  ) else (
rem current command?
rem можно сам интерпретатор попробовать вызвать	
    cmd /c "%L_KEY%"
    pause
  )
  %set_cd%
  %m-%save_env
	goto main_loop:
:end_ main_loop
