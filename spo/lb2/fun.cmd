@echo off & @setlocal ENABLEEXTENSIONS & @setlocal ENABLEDELAYEDEXPANSION & chcp 65001 >nul 2>&1

set L_CMDPATH=%~dp0 && set L_CMDPATH=!L_CMDPATH:~0,-2!
set env=call %L_CMDPATH%\env.cmd

:: ------------------------------------------------------------------------------
:: the functions list section
:: ------------------------------------------------------------------------------
if ".%1" equ "." ( 
	echo Do not derect call %0 && exit /b 1 
) else if ".%1" equ ".-fun1" (	
  rem test
  call :fun1 %2 %3 
) else if ".%1" equ ".-refresh" ( 
	rem refresh screen
	call :refresh 
) else if ".%1" equ ".-save_env" ( 
	rem save environment
	call :save_env 
) else if ".%1" equ ".-get_help" ( 
	rem help
	call :get_help 
) else if ".%1" equ ".-get_files_count" ( 
	rem files count
	call :get_files_count 
)

exit /b %errorlevel%

:: ------------------------------------------------------------------------------
:: for test fun1
:: ------------------------------------------------------------------------------
:fun1
  echo %L_KEY%
  set L_KEY=dd
  echo %L_KEY%
  call :save_env
	exit /b 2
:end_ fun1

:: ------------------------------------------------------------------------------
:: save current environment
:: ------------------------------------------------------------------------------
:save_env
  echo set L_TITLE=Saved environment>%L_CMDPATH%\env.cmd
  for /F "usebackq tokens=1* delims=" %%i in (`set L_`) do (
  	echo set %%i>>%L_CMDPATH%\env.cmd
  )
  exit /b
:end_ save_env

:: ------------------------------------------------------------------------------
:: refresh screen
:: ------------------------------------------------------------------------------
:refresh
	cls
	%env%
	set l_
	set _LINE=Path: %L_CD%                                                                             ║
	set _LINE=%_LINE:~0,77%

:: default lines of show
	if ".%L_CURLINE%" equ "." set /a L_CURLINE=1
	if %L_CURLINE% lss 1 set /a L_CURLINE=1
	if %L_CURLINE% gtr %L_CFG_HEIGHT% set /a L_CURLINE=%L_CFG_HEIGHT%
:: header
	echo ╔═════════════════════════════════════════════════════════════════════════════╗
  echo ║%_LINE%║
  echo ║                                                                             ║

rem число файлов
	set _FILESINFO=
  for /F "usebackq skip=5 tokens=1* delims=" %%i in (`dir %L_CD%`) do (
    set _CHK=%%i && set _CHK=!_CHK:~17,3!
  	if ".!_CHK!" equ ".Dir" (
  		set _DIRSINFO=%%i
  	) else if ".!_CHK!" equ ".Fil" (
  		set _FILESINFO=%%i
    )
  )
  set _COUNTER=1
  set _i=0
  for /F "usebackq tokens=1* delims=" %%i in (`dir /b %L_DEF_DIR_ORDER% %L_CD%`) do (
		set /a _i=!_i!+1
		if !_i! lss %L_TOPFILE% (
		  rem NOP
		) else (
  		set _FILE="%L_CD%\%%i"
  		set _OUTLINE=
  		call :format_outline !_FILE:\\=\!
rem  		 %%~ti %%~zi
 		  set _SEL= •
  		if !_COUNTER! equ %L_CURLINE% (
rem save fileinfo
  			call :set_current_file !_FILE:\\=\!
  			set _SEL=•»
  		)

    	if !_COUNTER! leq %L_CFG_HEIGHT% (
    		set _LINE=║ !_SEL! !_OUTLINE!                                                                             .
    		set _LINE=!_LINE:~0,78!
    		echo !_LINE!║
    	)
    	set /a _COUNTER=!_COUNTER!+1
  	)
 	)
 	set /a _FILL=%_COUNTER%
 	for /L %%i in (%_FILL%,1,%L_CFG_HEIGHT%) do (
 	  echo ║                                                                             ║
 	  set /a _COUNTER=!_COUNTER!+1
  )

:: footer
  echo ╟──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬───────────╢
	echo ║ q - exit │ w - prev │ s - next │ a - pgup │ d - pgdn │ ? - help │ : - run   ║
	echo ╚══════════╧══════════╧══════════╧══════════╧══════════╧══════════╧═══════════╝

	echo %_DIRSINFO%
	echo %_FILESINFO%
	echo •» %L_FILE%  %L_FILETIME% %L_FILEATTR% %L_FILESIZE%
	if ".%L_ERR%" neq "." echo ERROR: %L_ERR%
	call :save_env

	exit /b 0
:end_ refresh

:: ------------------------------------------------------------------------------
:: files count in current dir
:: ------------------------------------------------------------------------------
:get_files_count
  set L_FILESCOUNT=0
  for /F "usebackq tokens=1* delims=" %%i in (`dir /b`) do set /a L_FILESCOUNT=!L_FILESCOUNT!+1
  call :save_env
	exit /b 0
:end_ get_files_count

:: ------------------------------------------------------------------------------
:: format file line
:: ------------------------------------------------------------------------------
:format_outline
	set _OUT=%~nx1                                                 .
	set _ATTR=%~a1
	set _OUTLINE=%_OUT:~0,20% %_ATTR:~0,1% %~t1 %~z1
	exit /b 0
:end_ format_outline

:: ------------------------------------------------------------------------------
:: current file properties
:: ------------------------------------------------------------------------------
:set_current_file
	set L_FILE=%~nx1
	set L_FILETIME=%~t1
	set L_FULLPATH=%~f1
	set L_FILEPATH=%~p1
	set L_FILEATTR=%~a1
	set L_FILEEXT=%~x1
	set L_FILESIZE=%~z1
	exit /b 0
:end_ set_current_file

:: ------------------------------------------------------------------------------
:: get_help
:: ------------------------------------------------------------------------------
:get_help
	cls
	set l_
	echo ╔═════════════════════════════════════════════════════════════════════════════╗
  echo ║  Here is hepl for this batch routines                                       ║
  echo ║                                                                             ║
  echo ║  some text...                                                               ║
  echo ║                                                                             ║
  echo ║                                                                             ║
  echo ║                                                                             ║
  echo ║                                                                             ║
  echo ║                                                                             ║
  echo ║                                                                             ║
  echo ║                                                                             ║
  echo ╟─────────────────────────────────────────────────────────────────────────────╢
	echo ║ Press any key for continue                                                  ║
	echo ╚═════════════════════════════════════════════════════════════════════════════╝
	pause>nul

	exit /b 0
:end_ get_help
