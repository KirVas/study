package win_lab;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.MalformedInputException;

public class Win_Lab extends Frame implements ActionListener {
    Button bex = new Button("Exit");
    Button sea = new Button("Search");
    Button top = new Button("Top in browser");
    TextArea txa = new TextArea();
    int currentTopValue = -1;
    File currentTopFile = null;

    public Win_Lab() {
        super("my window");
        setLayout(null);
        setBackground(new Color(150, 200, 100));
        setSize(750, 250);
        add(bex);
        add(top);
        add(sea);
        add(txa);
        bex.setBounds(110, 190, 100, 20);
        bex.addActionListener(this);
        sea.setBounds(110, 165, 100, 20);
        sea.addActionListener(this);
        top.setBounds(310, 165, 100, 20);
        top.addActionListener(this);
        top.setVisible(false);
        txa.setBounds(20, 50, 700, 100);
        this.show();
        this.setLocationRelativeTo(null);
    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == bex) System.exit(0);
        else if (ae.getSource() == sea) {
            String[] keywords = txa.getText().split(",");
            for (int j = 0; j < keywords.length; j++) {
                System.out.println(keywords[j]);
            }
            File f = new File("C:/Users/Kiril/IdeaProjects/Lb1/src/source_html");
            ArrayList<File> files = new ArrayList<>(Arrays.asList(f.listFiles()));
            txa.setText("");
            for (File elem : files) {
                int zcoincidence = test_url(elem, keywords);
                txa.append("\n" + elem + "  :" + zcoincidence);
                if (zcoincidence > currentTopValue) {
                    currentTopValue = zcoincidence;
                    currentTopFile = elem;
                }
            }
            if (currentTopValue > 0) top.setVisible(true);
        } else if (ae.getSource() == top) {
            currentTopValue = -1;
            txa.setText("");
            openBrowser(currentTopFile);
            currentTopFile = null;
            top.setVisible(false);
        }
    }

    public static void openBrowser(File file) {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(file.toURI());
            } catch (IOException e) {
                System.out.println("error " + e.getMessage());
            }
        }
    }

    public static int test_url(File elem, String[] keywords) {
        int res = 0;
        URL url = null;
        URLConnection con = null;
        int i;
        try {
            String ffele = "" + elem;
            url = new URL("file:/" + ffele.trim());
            con = url.openConnection();
            File file = new File("C:/usr/Study/result.html");
            BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            String bhtml = "";


            while ((i = bis.read()) != -1) {
                bos.write(i);
                bhtml += (char) i;
            }
            bos.flush();
            bis.close();
            String htmlcontent = (new String(bhtml)).toLowerCase();
            System.out.println("New url content is: " + htmlcontent);
            for (int j = 0; j < keywords.length; j++) {
                if (htmlcontent.indexOf(keywords[j].trim().toLowerCase()) >= 0) res++;
            }
        } catch (MalformedInputException malformedInputException) {
            System.out.println("error " + malformedInputException.getMessage());
            return -1;
        } catch (IOException ioException) {
            System.out.println("error " + ioException.getMessage());
            return -1;
        } catch (Exception e) {
            System.out.println("error " + e.getMessage());
            return -1;
        }
        return res;
    }

    public static void main(String[] args) {
        new Win_Lab();
    }
}
