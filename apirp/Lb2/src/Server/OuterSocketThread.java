package Server;

import java.io.*;
import java.net.Socket;

public class OuterSocketThread extends Thread {

    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private Account account;

    public OuterSocketThread(Socket socket, Account account) throws IOException {
        this.socket = socket;
        this.account = account;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        start();
    }

    private void listen() {
        while (true) {
            try {
                sleep(100);
                String msg = in.readLine();
                if (msg != null) account.money();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void send(String msg) {
        try {
            out.write(msg + "\n");
            out.flush();
        } catch (IOException ignored) {
        }
    }

    @Override
    public void run() {
        listen();
    }
}
