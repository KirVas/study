package Server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class Account extends Thread {
    ServerSocket server;
    String amountstring;
    public volatile int amount = 200;

    private LinkedList<OuterSocketThread> outerSocketThreads = new LinkedList<>();

    public void run() {
        try {
            server = new ServerSocket(2525);
            while (true) {
                Socket s = null;
                try {
                    s = server.accept(); //ожидание соединения с клиентом
                    if (s != null) outerSocketThreads.add(new OuterSocketThread(s, this));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.out.println("Ошибка соединения+" + e);
        } finally {
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void money() {

        int amountcur = ((int) (Math.random() * 1000));// отрицательный вклад   снятие части денег со счета
        if (Math.random() > 0.5) amount -= amountcur;
        else amount += amountcur;

        for (OuterSocketThread outer : outerSocketThreads) {
            outer.send("Account: " + amount);
        }
    }


}