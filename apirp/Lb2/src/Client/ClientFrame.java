package Client;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientFrame extends Frame implements ActionListener {

    TextArea field = new TextArea();
    Button button = new Button("Money");
    ClientThread.FrameCallback callback;

    public ClientFrame(ClientThread.FrameCallback callback){
        super("Client");
        this.callback = callback;
        setLayout(null);
        setBackground(new Color(150,200,100));
        setSize(450,350);
        add(field);
        add(button);
        field.setBounds(50,50, 350, 100);
        button.setBounds(50,200, 350, 50);
        button.addActionListener(this);

    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == button) callback.pressed();
    }

    public void setText(String text){
        field.setText(text);
    }

    public boolean handleEvent(Event evt) {
        if (evt.id == Event.WINDOW_DESTROY) {
            System.exit(0);
        }
        return super.handleEvent(evt);
    }
}
