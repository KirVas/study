package Client;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class ClientThread extends Thread {
    DataInputStream dis = null;
    Socket s = null;
    ClientFrame frame = null;


    public ClientThread() {
        try {
            s = new Socket("127.0.0.1", 2525);
            dis = new DataInputStream(s.getInputStream());
            frame = new ClientFrame(this::money);
            frame.show();
        } catch (Exception e) {
            System.out.println("Ошибка:  " + e);
        }
    }

    public void run() {
        while (true) {
            try {
                sleep(100);
            } catch (Exception er) {
                System.out.println("Ошибка  " + er);
            }
            try {
                String msg = dis.readLine();
                if (msg == null) break;
                System.out.println(msg);
                frame.setText(msg);
            } catch (Exception e) {
                System.out.println("ERRORR+" + e);
            }
        }
    }

    private void money(){
        PrintStream ps = null; //PrintStream предназначен для текстового вывода
        try {
            ps = new PrintStream(s.getOutputStream());
            ps.println("change");
            ps.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    interface FrameCallback {
        void pressed();
    }
}
