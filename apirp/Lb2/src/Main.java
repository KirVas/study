import Client.ClientThread;
import Server.Account;

import java.awt.*;

public class Main extends Frame {
    public boolean handleEvent(Event evt) {
        if (evt.id == Event.WINDOW_DESTROY) {
            System.exit(0);
        }
        return super.handleEvent(evt);
    }

    public boolean mouseDown(Event evt, int x, int y) {
        new ClientThread().start();//Запуск потока клиента
        return (true);
    }

    public static void main(String args[]) {
        Main f = new Main();
        f.resize(400, 400);
        f.show();
        new Account().start();
    }
}
