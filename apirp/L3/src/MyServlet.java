

import com.microsoft.sqlserver.jdbc.SQLServerDriver;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet("/hello")
public class MyServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)  {
        try {
            response.setContentType("text/html; charset=UTF-8");
            DriverManager.registerDriver(new SQLServerDriver());
            String dbURL = "Server=DESKTOP-O772TG2\\\\KIRILMSSQL;Database=baseone; User Id = nuer; Password = zzzzzzzz;";
            Connection conn = null;
            conn = DriverManager.getConnection(dbURL);
            if (conn != null) {
                System.out.println("Connected");
            }
            String rus_word = "" + request.getParameter("txt");
            String eng_word = "" + request.getParameter("trans");
            ResultSet ru_set = null;
            ResultSet en_set = null;
            en_set = conn.prepareStatement("select en from translate where (ru='" + rus_word +"')").executeQuery();
            en_set.first();
            String en_tr = en_set.getString(1);
            request.setAttribute("trans",  (en_tr == null ? en_tr : ""));
            ru_set = conn.prepareStatement("select ru from translate where (en='" + eng_word +"')").executeQuery();
            ru_set.first();
            String ru_tr = ru_set.getString(1);
            request.setAttribute("txt", (ru_tr == null ? ru_tr : ""));
            getServletContext().getRequestDispatcher("/index.jsp").forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}