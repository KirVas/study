
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/hello")
public class MyServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
//        String rus_word = "" + request.getParameter("txt");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet MyServletStud</title>");
        out.println("</head>");
        out.println("<body bgcolor='#aaccff'");
        out.println("<form>");
        out.println("<h2> Привет клиенту!!!</h2><br><br>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");
        out.flush();
    }
}