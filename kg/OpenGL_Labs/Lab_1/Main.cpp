#include <iostream>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <wingdi.h>

// Include GLM
// #include <glm/glm.hpp>
// using namespace glm;

static HGLRC hRC; // Постоянный контекст рендеринга
static HDC hDC; // Приватный контекст устройства GDI

BOOL keys[256]; // массив для обработки клавиатуры

/*
 * Вызвать после создания окна GL
 */
GLvoid InitGL(GLsizei width, GLsizei height)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Очищает экран в чёрный цвет
}

GLvoid ResizeGLScene(GLsizei width, GLsizei height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, width, height);
}

/*
 * Функция предназначена для рисования сцены
 */
GLvoid DrawGLScene(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT); // очистка экрана

}

GLvoid DrawFirstGLScene(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT); // очистка экрана

	glPointSize(3); //размер точки

	glEnable(GL_POINT_SMOOTH);

	glBegin(GL_POINTS); // Коммандные "скобки"

	glColor3d(1, 1, 0.9);

	glVertex2d(0.2, -0.8);

	glColor3d(0.4, 0.2, 0.9);

	glVertex2d(0.3, 0.5);

	glEnd();

	glDisable(GL_POINT_SMOOTH);
}

GLvoid DrawSecondGLScene(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT); // очистка экрана

	glPointSize(5); //размер точки

	glEnable(GL_LINE_SMOOTH);

	glBegin(GL_LINE_STRIP); // Коммандные "скобки"

	glColor3d(1, 1, 0.9);

	glVertex2d(-0.95, 0.9);

	glVertex2d(0, 0.9);

	glColor3d(0.4, 0.2, 0.9);

	glVertex2d(-0.75, 0.55);

	glColor3d(0.3, 0.1, 0.2);

	glVertex2d(0.2, 0.2);

	glColor3d(0.1, 0.5, 0.3);
	
	glVertex2d(-0.95, 0.2);

	glColor3d(0.4, 0.2, 0.3);

	glVertex2d(-0.75, 0);

	glColor3d(0.4, 0.4, 0.5);

	glVertex2d(-0.95, -0.2);

	glColor3d(0.5, 0.2, 0.9);

	glEnd();

	glDisable(GL_LINE_SMOOTH);
}

GLvoid DrawThirdGLScene(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT); // очистка экрана

	glPointSize(5); //размер точки

	glEnable(GL_LINE_SMOOTH);

	glBegin(GL_LINE_LOOP); // Коммандные "скобки"

	glColor3d(1, 1, 0.9);

	glVertex2d(-0.8, 0.9);

	glVertex2d(0.3, 0.9);

	glColor3d(0.4, 0.2, 0.9);

	glVertex2d(0.35, 0.7);

	glColor3d(0.3, 0.1, 0.2);

	glVertex2d(0.35, 0);

	glColor3d(0.1, 0.5, 0.3);

	glVertex2d(0.1, -0.1);

	glColor3d(0.4, 0.2, 0.3);

	glVertex2d(0, 0.4);

	glColor3d(0.4, 0.4, 0.5);

	glVertex2d(-0.8, -0.1);

	glColor3d(0.5, 0.2, 0.9);

	glVertex2d(-0.75, 0.7);

	glColor3d(0.5, 0.2, 0.9);

	glVertex2d(-0.95, 0.8);

	glColor3d(0.5, 0.2, 0.9);


	glEnd();

	glDisable(GL_LINE_SMOOTH);
}

GLvoid DrawFourthGLScene(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT); // очистка экрана

	glPointSize(5); //размер точки

	glBegin(GL_QUAD_STRIP); // Коммандные "скобки"

	glColor3d(0.1, 0.5, 0.9);

	glVertex2d(-0.95, 0.9);

	glVertex2d(-0.85, 0.4);

	glVertex2d(0.4, 0.8);

	glVertex2d(0, 0.4);
	


	glColor3d(0.3, 0.1, 0.2);

	glVertex2d(0.4, -0.1);

	glVertex2d(0, 0);


	glColor3d(0.6, 0.9, 0.3);

	glVertex2d(-0.2, -0.2);

	glVertex2d(-0.3, 0);


	glColor3d(0.3, 0.1, 0.2);

	glVertex2d(-0.85, -0.2);

	glVertex2d(-0.7, 0.3);

	glEnd();

}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT Screen;
	GLuint PixelFormat;
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR), // Размер этой структуры
		1, // Номер версии
		PFD_DRAW_TO_WINDOW | // Формат для Окна
		PFD_SUPPORT_OPENGL | // Формат для OpenGL
		PFD_DOUBLEBUFFER, // Формат для двойного буфера
		PFD_TYPE_RGBA, // Требуется RGBA формат
		16, // Выбор 16 бит глубины цвета
		0, 0, 0, 0, 0, 0, // Игнорирование цветовых битов
		0, // нет буфера прозрачности
		0, // Сдвиговый бит игнорируется
		0, // Нет буфера аккумуляции
		0, 0, 0, 0, // Биты аккумуляции игнорируются
		16, // 16 битный Z-буфер (буфер глубины)
		0, // Нет буфера трафарета
		0, // Нет вспомогательных буферов
		PFD_MAIN_PLANE, // Главный слой рисования
		0, // Резерв
		0, 0, 0 // Маски слоя игнорируются
	};

	switch (message)
	{
	case WM_CREATE:

		hDC = GetDC(hWnd); // Получить контекст устройства для окна

		PixelFormat = ChoosePixelFormat(hDC, &pfd);
		if (!PixelFormat)

		{
			MessageBox(0, "Не найден подходящий формат пиксела.", "Error", MB_OK | MB_ICONERROR);

			PostQuitMessage(0);

			// Это сообщение говорит, что программа должна завершится

			break; // Предтовращение повтора кода
		}
		if (!SetPixelFormat(hDC, PixelFormat, &pfd))
		{
			MessageBox(0, "Формат пиксела не установлен.", "Error", MB_OK | MB_ICONERROR);

			PostQuitMessage(0);

			break;
		}
		hRC = wglCreateContext(hDC);

		if (!hRC)
		{
			MessageBox(0, "Контекст воспроизведения не создан.", "Error", MB_OK | MB_ICONERROR);

			PostQuitMessage(0);

			break;
		}

		if (!wglMakeCurrent(hDC, hRC))

		{
			MessageBox(nullptr, "Невозможно активизировать GLRC.", "Error", MB_OK | MB_ICONERROR);

			PostQuitMessage(0);

			break;
		}
		GetClientRect(hWnd, &Screen);

		InitGL(Screen.right, Screen.bottom);

		break;
	case WM_DESTROY:

	case WM_CLOSE:

		ChangeDisplaySettings(nullptr, 0);

		wglMakeCurrent(hDC, nullptr);

		wglDeleteContext(hRC);

		ReleaseDC(hWnd, hDC);

		PostQuitMessage(0);

		break;
	case WM_KEYDOWN:
		keys[wParam] = TRUE;

		break;
	case WM_KEYUP:
		keys[wParam] = FALSE;

		break;
	case WM_SIZE:
		ResizeGLScene(LOWORD(lParam), HIWORD(lParam));

		break;
	default:

		return (DefWindowProc(hWnd, message, wParam, lParam));
	}

	return (0);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg; // Структура сообщения Windows

	WNDCLASS wc; // Структура класса Windows для установки типа окна

	HWND hWnd; // Сохранение дескриптора окна
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	wc.lpfnWndProc = (WNDPROC)WndProc;

	wc.cbClsExtra = 0;

	wc.cbWndExtra = 0;

	wc.hInstance = hInstance;

	wc.hIcon = NULL;

	wc.hCursor = LoadCursor(NULL, IDC_ARROW);

	wc.hbrBackground = NULL;

	wc.lpszMenuName = NULL;

	wc.lpszClassName = "OpenGL WinClass";
	if (!RegisterClass(&wc))
	{
		MessageBox(0, "Error регистрации класса окна.", "Error", MB_OK | MB_ICONERROR);

		return FALSE;
	}
	hWnd = CreateWindow(
		"OpenGL WinClass",
		"OpenGL", // Заголовок вверху окна
		WS_POPUP |
		WS_CLIPCHILDREN |
		WS_CLIPSIBLINGS,
		10, 10, // Позиция окна на экране
		500, 500, // Ширина и высота окна
		NULL,
		NULL,
		hInstance,
		NULL);
	if (!hWnd)
	{
		MessageBox(0, "Error создания окна.", "Error", MB_OK | MB_ICONERROR);

		return FALSE;
	}
	DEVMODE dmScreenSettings; // Режим работы

	memset(&dmScreenSettings, 0, sizeof(DEVMODE));

	// Очистка для хранения установок
	dmScreenSettings.dmSize = sizeof(DEVMODE); // Размер структуры Devmode

	dmScreenSettings.dmPelsWidth = 300; // Ширина экрана
	dmScreenSettings.dmPelsHeight = 300; // Высота экрана

	dmScreenSettings.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT; // Режим Пиксела

	ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
	ShowWindow(hWnd, SW_SHOW);

	UpdateWindow(hWnd);

	SetFocus(hWnd);
	while (true)
	{
		// Обработка всех сообщений
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if (GetMessage(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);

				DispatchMessage(&msg);
			}
			else
			{
				return TRUE;
			}
		}

		DrawGLScene(); // Нарисовать сцену

		

		if (keys[VK_ESCAPE])
		{
			SendMessage(hWnd, WM_CLOSE, 0, 0);
		} 
		// Если ESC - выйти

		if (keys[VK_NUMPAD1]) DrawFirstGLScene();
		else if (keys[VK_NUMPAD2]) DrawSecondGLScene();
		else if (keys[VK_NUMPAD3]) DrawThirdGLScene();
		else if (keys[VK_NUMPAD4]) DrawFourthGLScene();
		else if (keys[VK_NUMPAD5]) DrawThirdGLScene();

		else DrawGLScene();

		SwapBuffers(hDC); // Переключить буфер экрана
	}
}
