#include <iostream>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

static HGLRC hRC; // Постоянный контекст рендеринга
static HDC hDC; // Приватный контекст устройства GDI
BOOL keys[256]; // Массив для процедуры обработки клавиатуры

GLvoid InitGL(GLsizei Width, GLsizei Height) // Вызвать после создания окна GL
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Очистка экрана в черный цвет
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
}

GLvoid ReSizeGLScene(GLsizei Width, GLsizei Height)
{
	if (Height == 0)
	{
		// Предотвращение деления на ноль, 
		//если окно слишком мало
		Height = 1;
	}

	glViewport(0, 0, Width, Height); // Сброс текущей области вывода 
}

GLvoid DrawGLScene(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT); // Очистка экрана

	GLfloat triangleVertexArray[7][3];
	GLfloat triangleColorArray[7][3];
	GLubyte triangleIndexArray[3][3];

	triangleVertexArray[0][0] = 0;
	triangleVertexArray[0][1] = 0;
	triangleVertexArray[0][2] = 0.0;

	triangleVertexArray[1][0] = 0.2;
	triangleVertexArray[1][1] = 0.5;
	triangleVertexArray[1][2] = 0.0;

	triangleVertexArray[2][0] = 0.5;
	triangleVertexArray[2][1] = 0.2;
	triangleVertexArray[2][2] = 0;

	triangleVertexArray[3][0] = -0.2;
	triangleVertexArray[3][1] = 0.5;
	triangleVertexArray[3][2] = 0.0;

	triangleVertexArray[4][0] = -0.5;
	triangleVertexArray[4][1] = 0.2;
	triangleVertexArray[4][2] = 0.0;

	triangleVertexArray[5][0] = 0.3;
	triangleVertexArray[5][1] = -0.3;
	triangleVertexArray[5][2] = 0.0;

	triangleVertexArray[6][0] = -0.3;
	triangleVertexArray[6][1] = -0.3;
	triangleVertexArray[6][2] = 0.0;

	triangleColorArray[0][0] = 1;
	triangleColorArray[0][1] = 1;
	triangleColorArray[0][2] = 1;

	triangleColorArray[1][0] = 1;
	triangleColorArray[1][1] = 0.87;
	triangleColorArray[1][2] = 0;

	triangleColorArray[2][0] = 0.25;
	triangleColorArray[2][1] = 0.7;
	triangleColorArray[2][2] = 0.1;

	triangleColorArray[3][0] = 0.75;
	triangleColorArray[3][1] = 0.14;
	triangleColorArray[3][2] = 0;

	triangleColorArray[4][0] = 0.95;
	triangleColorArray[4][1] = 0.5;
	triangleColorArray[4][2] = 0.31;

	triangleColorArray[5][0] = 1;
	triangleColorArray[5][1] = 0;
	triangleColorArray[5][2] = 0.31;

	triangleColorArray[6][0] = 0;
	triangleColorArray[6][1] = 0;
	triangleColorArray[6][2] = 1;

	triangleIndexArray[0][0] = 0;
	triangleIndexArray[0][1] = 1;
	triangleIndexArray[0][2] = 2;

	triangleIndexArray[1][0] = 0;
	triangleIndexArray[1][1] = 3;
	triangleIndexArray[1][2] = 4;

	triangleIndexArray[2][0] = 0;
	triangleIndexArray[2][1] = 5;
	triangleIndexArray[2][2] = 6;


	glVertexPointer(3, GL_FLOAT, 0, triangleVertexArray);
	glColorPointer(3, GL_FLOAT, 0, triangleColorArray);
	glDrawElements(GL_TRIANGLES, 9, GL_UNSIGNED_BYTE, triangleIndexArray);
	// ROTATE
	glRotatef(-0.5, 1, 0, 1);

	glEnd();
	// здесь закончилось создание рисунка
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT Screen; // используется позднее для размеров окна
	GLuint PixelFormat;
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR), // Размер этой структуры
		1, // Номер версии (?)
		PFD_DRAW_TO_WINDOW | // Формат для Окна
		PFD_SUPPORT_OPENGL | // Формат для OpenGL
		PFD_DOUBLEBUFFER, // Формат для двойного буфера
		PFD_TYPE_RGBA, // Требуется RGBA формат
		16, // Выбор 16 бит глубины цвета
		0, 0, 0, 0, 0, 0, // Игнорирование цветовых битов (?)
		0, // нет буфера прозрачности
		0, // Сдвиговый бит игнорируется (?)
		0, // Нет буфера аккумуляции
		0, 0, 0, 0, // Биты аккумуляции игнорируются (?)
		16, // 16 битный Z-буфер (буфер глубины)
		0, // Нет буфера траффарета
		0, // Нет вспомогательных буферов (?)
		PFD_MAIN_PLANE, // Главный слой рисования
		0, // Резерв (?)
		0, 0, 0 // Маски слоя игнорируются (?)
	};
	switch (message) // Тип сообщения
	{
	case WM_CREATE:
		hDC = GetDC(hWnd); // Получить контекст устройства для окна
		PixelFormat = ChoosePixelFormat(hDC, &pfd);
		// Найти ближайшее совпадение для нашего формата пикселов
		if (!PixelFormat)
		{
			MessageBox(0, "Can't Find A SuitablePixelFormat.", "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			// Это сообщение говорит, что программа должна завершиться
			break; // Предтовращение повтора кода
		}
		if (!SetPixelFormat(hDC, PixelFormat, &pfd))
		{
			MessageBox(0, "Can't Set ThePixelFormat.", "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			break;
		}
		hRC = wglCreateContext(hDC);
		if (!hRC)
		{
			MessageBox(0,
			           "Can't Create A GLRenderingContext.",
			           "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			break;
		}
		if (!wglMakeCurrent(hDC, hRC))
		{
			MessageBox(0, "Can't activate GLRC.", "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			break;
		}
		GetClientRect(hWnd, &Screen);
		InitGL(Screen.right, Screen.bottom);
		break;
	case WM_DESTROY:
	case WM_CLOSE:
		ChangeDisplaySettings(NULL, 0);
		wglMakeCurrent(hDC, NULL);
		wglDeleteContext(hRC);
		ReleaseDC(hWnd, hDC);
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		keys[wParam] = TRUE;
		break;
	case WM_KEYUP:
		keys[wParam] = FALSE;
		break;
	case WM_SIZE:
		ReSizeGLScene(LOWORD(lParam), HIWORD(lParam));
		break;
	default:
		return (DefWindowProc(hWnd, message, wParam, lParam));
	}
	return (0);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg; // Структура сообщения Windows
	WNDCLASS wc; // Структура класса Windows для установки типа окна
	HWND hWnd; // Сохранение дескриптора окна
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = NULL;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "OpenGL WinClass";
	if (!RegisterClass(&wc))
	{
		MessageBox(0,
		           "Failed To Register The WindowClass.",
		           "Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}
	hWnd = CreateWindow("OpenGL WinClass",
	                    "First OpenGL program", // Заголовок вверху окна
	                    WS_POPUP |
	                    WS_CLIPCHILDREN |
	                    WS_CLIPSIBLINGS,
	                    0, 0, // Позиция окна на экране
	                    1024, 1024, // Ширина и высота окна
	                    NULL,
	                    NULL,
	                    hInstance,
	                    NULL);
	if (!hWnd)
	{
		MessageBox(0, "Window Creation Error.", "Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}
	if (!hWnd)
	{
		MessageBox(0, "Window Creation Error.", "Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}

	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
	SetFocus(hWnd);
	while (true)
	{
		// Обработка всех сообщений
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if (GetMessage(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				return TRUE;
			}
		}

		DrawGLScene(); // Нарисовать сцену
		SwapBuffers(hDC); // Переключить буфер экрана
		if (keys[VK_ESCAPE])
			SendMessage(hWnd, WM_CLOSE, 0, 0); // Если ESC - выйти
	}
}
