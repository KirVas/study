#include <iostream>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

static HGLRC hRC; // Постоянный контекст рендеринга
static HDC hDC; // Приватный контекст устройства GDI
BOOL keys[256]; // Массив для процедуры обработки клавиатуры

GLvoid InitGL(GLsizei Width, GLsizei Height) // Вызвать после создания окна GL
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Очистка экрана в черный цвет
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glShadeModel(GL_FLAT);
	glEnable(GL_CULL_FACE);
}

GLvoid ReSizeGLScene(GLsizei Width, GLsizei Height)
{
	if (Height == 0) // Предотвращение деления на ноль,
	{
		//если окно слишком мало
		Height = 1;
	}

	glViewport(0, 0, Width, Height); // Сброс текущей области вывода 
}

GLvoid DrawGLScene(GLvoid)
{
	glClear(GL_COLOR_BUFFER_BIT); // Очистка экрана

	GLfloat cubeVertexArray[8][3];
	GLfloat cubeColorArray[8][3];
	GLubyte cubeIndexArray[6][4];

	cubeVertexArray[0][0] = 0.0;
	cubeVertexArray[0][1] = 0.0;
	cubeVertexArray[0][2] = 0.5;

	cubeVertexArray[1][0] = 0.0;
	cubeVertexArray[1][1] = 0.5;
	cubeVertexArray[1][2] = 0.5;

	cubeVertexArray[2][0] = 0.5;
	cubeVertexArray[2][1] = 0.5;
	cubeVertexArray[2][2] = 0.5;

	cubeVertexArray[3][0] = 0.5;
	cubeVertexArray[3][1] = 0.0;
	cubeVertexArray[3][2] = 0.5;

	cubeVertexArray[4][0] = 0.0;
	cubeVertexArray[4][1] = 0.0;
	cubeVertexArray[4][2] = 0.0;

	cubeVertexArray[5][0] = 0.0;
	cubeVertexArray[5][1] = 0.5;
	cubeVertexArray[5][2] = 0.0;

	cubeVertexArray[6][0] = 0.5;
	cubeVertexArray[6][1] = 0.5;
	cubeVertexArray[6][2] = 0.0;

	cubeVertexArray[7][0] = 0.5;
	cubeVertexArray[7][1] = 0.0;
	cubeVertexArray[7][2] = 0.0;

	cubeColorArray[0][0] = 0.0;
	cubeColorArray[0][1] = 0.0;
	cubeColorArray[0][2] = 1.0;

	cubeColorArray[1][0] = 0.6;
	cubeColorArray[1][1] = 0.98;
	cubeColorArray[1][2] = 0.6;

	cubeColorArray[2][0] = 1.0;
	cubeColorArray[2][1] = 0.84;
	cubeColorArray[2][2] = 0.8;

	cubeColorArray[3][0] = 0.8;
	cubeColorArray[3][1] = 0.36;
	cubeColorArray[3][2] = 0.36;

	cubeColorArray[4][0] = 1.0;
	cubeColorArray[4][1] = 0.27;
	cubeColorArray[4][2] = 0.0;

	cubeColorArray[5][0] = 0.82;
	cubeColorArray[5][1] = 0.13;
	cubeColorArray[5][2] = 0.56;

	cubeColorArray[6][0] = 0.54;
	cubeColorArray[6][1] = 0.17;
	cubeColorArray[6][2] = 0.89;

	cubeColorArray[7][0] = 0.0;
	cubeColorArray[7][1] = 1.0;
	cubeColorArray[7][2] = 1.0;

	cubeIndexArray[0][0] = 0;
	cubeIndexArray[0][1] = 3;
	cubeIndexArray[0][2] = 2;
	cubeIndexArray[0][3] = 1;

	cubeIndexArray[1][0] = 0;
	cubeIndexArray[1][1] = 1;
	cubeIndexArray[1][2] = 5;
	cubeIndexArray[1][3] = 4;

	cubeIndexArray[2][0] = 7;
	cubeIndexArray[2][1] = 4;
	cubeIndexArray[2][2] = 5;
	cubeIndexArray[2][3] = 6;

	cubeIndexArray[3][0] = 3;
	cubeIndexArray[3][1] = 7;
	cubeIndexArray[3][2] = 6;
	cubeIndexArray[3][3] = 2;

	cubeIndexArray[4][0] = 1;
	cubeIndexArray[4][1] = 2;
	cubeIndexArray[4][2] = 6;
	cubeIndexArray[4][3] = 5;

	cubeIndexArray[5][0] = 0;
	cubeIndexArray[5][1] = 4;
	cubeIndexArray[5][2] = 7;
	cubeIndexArray[5][3] = 3;

	glVertexPointer(3, GL_FLOAT, 0, cubeVertexArray);
	glColorPointer(3, GL_FLOAT, 0, cubeColorArray);
	glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndexArray);

	glRotatef(0.1, 0, 1, 1);

	glEnd();
	// здесь закончилось создание рисунка
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT Screen; // используется позднее для размеров окна
	GLuint PixelFormat;
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR), // Размер этой структуры
		1, // Номер версии (?)
		PFD_DRAW_TO_WINDOW | // Формат для Окна
		PFD_SUPPORT_OPENGL | // Формат для OpenGL
		PFD_DOUBLEBUFFER, // Формат для двойного буфера
		PFD_TYPE_RGBA, // Требуется RGBA формат
		16, // Выбор 16 бит глубины цвета
		0, 0, 0, 0, 0, 0, // Игнорирование цветовых битов (?)
		0, // нет буфера прозрачности
		0, // Сдвиговый бит игнорируется (?)
		0, // Нет буфера аккумуляции
		0, 0, 0, 0, // Биты аккумуляции игнорируются (?)
		16, // 16 битный Z-буфер (буфер глубины)
		0, // Нет буфера траффарета
		0, // Нет вспомогательных буферов (?)
		PFD_MAIN_PLANE, // Главный слой рисования
		0, // Резерв (?)
		0, 0, 0 // Маски слоя игнорируются (?)
	};
	switch (message) // Тип сообщения
	{
	case WM_CREATE:
		hDC = GetDC(hWnd); // Получить контекст устройства для окна
		PixelFormat = ChoosePixelFormat(hDC, &pfd);
		// Найти ближайшее совпадение для нашего формата пикселов
		if (!PixelFormat)
		{
			MessageBox(0, "Can't Find A SuitablePixelFormat.", "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			// Это сообщение говорит, что программа должна завершиться
			break; // Предтовращение повтора кода
		}
		if (!SetPixelFormat(hDC, PixelFormat, &pfd))
		{
			MessageBox(0, "Can't Set ThePixelFormat.", "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			break;
		}
		hRC = wglCreateContext(hDC);
		if (!hRC)
		{
			MessageBox(0,
			           "Can't Create A GLRenderingContext.",
			           "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			break;
		}
		if (!wglMakeCurrent(hDC, hRC))
		{
			MessageBox(0, "Can't activate GLRC.", "Error", MB_OK | MB_ICONERROR);
			PostQuitMessage(0);
			break;
		}
		GetClientRect(hWnd, &Screen);
		InitGL(Screen.right, Screen.bottom);
		break;
	case WM_DESTROY:
	case WM_CLOSE:
		ChangeDisplaySettings(NULL, 0);

		wglMakeCurrent(hDC, NULL);
		wglDeleteContext(hRC);
		ReleaseDC(hWnd, hDC);
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		keys[wParam] = TRUE;
		break;

	case WM_KEYUP:
		keys[wParam] = FALSE;
		break;
	case WM_SIZE:
		ReSizeGLScene(LOWORD(lParam), HIWORD(lParam));
		break;
	default:
		return (DefWindowProc(hWnd, message, wParam, lParam));
	}
	return (0);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg; // Структура сообщения Windows
	WNDCLASS wc; // Структура класса Windows для установки типа окна
	HWND hWnd; // Сохранение дескриптора окна
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = NULL;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "OpenGL WinClass";
	if (!RegisterClass(&wc))
	{
		MessageBox(0,
		           "Failed To Register The WindowClass.",
		           "Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}
	hWnd = CreateWindow("OpenGL WinClass",
	                    "First OpenGL program", // Заголовок вверху окна
	                    WS_POPUP |
	                    WS_CLIPCHILDREN |
	                    WS_CLIPSIBLINGS,
	                    0, 0, // Позиция окна на экране
	                    1080, 1080, // Ширина и высота окна
	                    NULL,
	                    NULL,
	                    hInstance,
	                    NULL);
	if (!hWnd)
	{
		MessageBox(0, "Window Creation Error.", "Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}
	if (!hWnd)
	{
		MessageBox(0, "Window Creation Error.", "Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}

	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
	SetFocus(hWnd);
	while (true)
	{
		// Обработка всех сообщений
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if (GetMessage(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				return TRUE;
			}
		}

		DrawGLScene(); // Нарисовать сцену
		SwapBuffers(hDC); // Переключить буфер экрана
		if (keys[VK_ESCAPE])
		{
			SendMessage(hWnd, WM_CLOSE, 0, 0); // Если ESC - выйти
		}
	}
}
