-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'User'
-- 
-- ---
use debatebase;

DROP TABLE IF EXISTS `User`;
		
CREATE TABLE `User` (
  `id` BIGINT AUTO_INCREMENT,
  `type` INT NULL DEFAULT NULL,
  `nickname` VARCHAR(25) NULL DEFAULT NULL,
  `last_active` DATETIME NULL DEFAULT NULL,
  `role_id` BIGINT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Guest'
-- 
-- ---

DROP TABLE IF EXISTS `Guest`;
		
CREATE TABLE `Guest` (
  `id` BIGINT AUTO_INCREMENT,
  `first_arrival` DATETIME NULL DEFAULT NULL,
  `fingerprint` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Profile'
-- 
-- ---

DROP TABLE IF EXISTS `Profile`;
		
CREATE TABLE `Profile` (
  `id` BIGINT AUTO_INCREMENT,
  `email` VARCHAR(50) NULL DEFAULT NULL,
  `password` VARCHAR(30) NULL DEFAULT NULL,
  `validated` BINARY NULL DEFAULT NULL,
  `info` VARCHAR(300) NULL DEFAULT NULL,
  `avatar` VARCHAR(200) NULL DEFAULT NULL,
  `first_arrival` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Debate'
-- 
-- ---

DROP TABLE IF EXISTS `Debate`;
		
CREATE TABLE `Debate` (
  `id` BIGINT AUTO_INCREMENT,
  `owner_id` BIGINT NULL DEFAULT NULL,
  `for_id` BIGINT NULL DEFAULT NULL,
  `against_id` BIGINT NULL DEFAULT NULL,
  `category_id` BIGINT NULL DEFAULT NULL,
  `connection` INT NULL DEFAULT NULL,
  `created` DATE NULL DEFAULT NULL,
  `info` VARCHAR(300) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Category'
-- 
-- ---

DROP TABLE IF EXISTS `Category`;
		
CREATE TABLE `Category` (
  `id` BIGINT AUTO_INCREMENT,
  `name` VARCHAR(30) NULL DEFAULT NULL,
  `custom` BINARY NULL DEFAULT NULL,
  `creator_id` BIGINT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Message'
-- 
-- ---

DROP TABLE IF EXISTS `Message`;
		
CREATE TABLE `Message` (
  `id` BIGINT AUTO_INCREMENT,
  `debate_id` BIGINT NULL DEFAULT NULL,
  `user_id` BIGINT NULL DEFAULT NULL,
  `message` VARCHAR(500) NULL DEFAULT NULL,
  `timestamp` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Like'
-- 
-- ---

DROP TABLE IF EXISTS `Like`;
		
CREATE TABLE `Like` (
  `id` BIGINT AUTO_INCREMENT,
  `user_id` BIGINT NULL DEFAULT NULL,
  `target_type` MEDIUMINT NULL DEFAULT NULL,
  `target_id` BIGINT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Viewer'
-- 
-- ---

DROP TABLE IF EXISTS `Viewer`;
		
CREATE TABLE `Viewer` (
  `id` BIGINT AUTO_INCREMENT,
  `user_id` BIGINT NULL DEFAULT NULL,
  `debate_id` BIGINT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'SavedDebate'
-- 
-- ---

DROP TABLE IF EXISTS `SavedDebate`;
		
CREATE TABLE `SavedDebate` (
  `id` BIGINT AUTO_INCREMENT,
  `debate_id` BIGINT NULL DEFAULT NULL,
  `user_id` BIGINT NULL DEFAULT NULL,
  `timestamp` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `User` ADD FOREIGN KEY (role_id) REFERENCES `Profile` (`id`);
ALTER TABLE `User` ADD FOREIGN KEY (role_id) REFERENCES `Guest` (`id`);
ALTER TABLE `Debate` ADD FOREIGN KEY (owner_id) REFERENCES `User` (`id`);
ALTER TABLE `Debate` ADD FOREIGN KEY (for_id) REFERENCES `User` (`id`);
ALTER TABLE `Debate` ADD FOREIGN KEY (against_id) REFERENCES `User` (`id`);
ALTER TABLE `Debate` ADD FOREIGN KEY (category_id) REFERENCES `Category` (`id`);
ALTER TABLE `Category` ADD FOREIGN KEY (creator_id) REFERENCES `User` (`id`);
ALTER TABLE `Message` ADD FOREIGN KEY (debate_id) REFERENCES `Debate` (`id`);
ALTER TABLE `Message` ADD FOREIGN KEY (user_id) REFERENCES `User` (`id`);
ALTER TABLE `Like` ADD FOREIGN KEY (user_id) REFERENCES `User` (`id`);
ALTER TABLE `Like` ADD FOREIGN KEY (target_id) REFERENCES `Message` (`id`);
ALTER TABLE `Like` ADD FOREIGN KEY (target_id) REFERENCES `User` (`id`);
ALTER TABLE `Like` ADD FOREIGN KEY (target_id) REFERENCES `Debate` (`id`);
ALTER TABLE `Viewer` ADD FOREIGN KEY (user_id) REFERENCES `User` (`id`);
ALTER TABLE `Viewer` ADD FOREIGN KEY (debate_id) REFERENCES `Debate` (`id`);
ALTER TABLE `SavedDebate` ADD FOREIGN KEY (debate_id) REFERENCES `Debate` (`id`);
ALTER TABLE `SavedDebate` ADD FOREIGN KEY (user_id) REFERENCES `User` (`id`);
 alter table `Profile` MODIFY COLUMN `validated` TINYINT(1);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `User` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Guest` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Profile` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Debate` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Category` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Message` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Like` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Viewer` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `SavedDebate` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

 INSERT INTO `User` (`type`,`nickname`,`last_active`,`role_id`) VALUES
 (1,'test1',NOW(6),2);
INSERT INTO `Guest` (`first_arrival`,`fingerprint`) VALUES
(NOW(6),'aabbbccfvdfc');
-- INSERT INTO `Profile` (`id`,`email`,`password`,`validated`,`info`,`avatar`,`first_arrival`) VALUES
-- ('','','','','','','');
-- INSERT INTO `Debate` (`id`,`owner_id`,`for_id`,`against_id`,`category_id`,`connection`,`created`,`info`) VALUES
-- ('','','','','','','','');
INSERT INTO `Category` (`name`,`custom`,`creator_id`) VALUES
('Карантин',0,NULL);
-- INSERT INTO `Message` (`id`,`debate_id`,`user_id`,`message`,`timestamp`) VALUES
-- ('','','','','');
-- INSERT INTO `Like` (`id`,`user_id`,`target_type`,`target_id`) VALUES
-- ('','','','');
-- INSERT INTO `Viewer` (`id`,`user_id`,`debate_id`) VALUES
-- ('','','');
-- INSERT INTO `SavedDebate` (`id`,`debate_id`,`user_id`,`timestamp`) VALUES
-- ('','','','');